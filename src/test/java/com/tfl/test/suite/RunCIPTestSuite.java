package com.tfl.test.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.tfl.test.runner.RunCIPSprint0Feature;


/**
 * Group any number of stories/sprints/features you would like to execute
 *
 */
@RunWith(Suite.class)
@SuiteClasses({RunCIPSprint0Feature.class})	//Run single feature
public class RunCIPTestSuite {

}
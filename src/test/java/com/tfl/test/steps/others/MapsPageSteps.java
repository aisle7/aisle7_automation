package com.tfl.test.steps.others;
import java.util.List;

import static org.junit.Assert.*;

import com.tfl.test.driver.manager.WebDriverManager;
import com.tfl.test.pages.MapsPage;
import com.tfl.test.pages.SSPPage;
import com.tfl.test.pages.TFLHomePage;
import com.tfl.test.steps.CommonSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MapsPageSteps extends CommonSteps {

	private static final String MAPS_PAGE = "maps"; 
	
	 
	@Given("^user on the maps page$")
	public void user_on_the_maps_page() throws Throwable {
		WebDriverManager.getSpecificSection(driver, MAPS_PAGE);
		mapsPage = new MapsPage(driver);
		
	}

	@When("^user click on Bus option from Maps page$")
	public void user_click_on_Bus_option_from_Maps_page() throws Throwable {
		mapsPage.clickOnBusFromMaps();
	}

	@Then("^The wording on the button should be View Spider maps$")
	public void the_wording_on_the_button_should_be_View_Spider_maps() throws Throwable {
		boolean isDisplayed = mapsPage.checkViewSpiderMapsTextShownInButton();
		assertTrue("Expected to see View Spider Maps text in search box", isDisplayed);
	}

	@Given("^user on the bus spider maps page$")
	public void user_on_the_bus_spider_maps_page() throws Throwable {
		WebDriverManager.getSpecificSection(driver, MAPS_PAGE);
		mapsPage = new MapsPage(driver);
		mapsPage.clickOnBusFromMaps();
		mapsPage.clickOnBusSpiderMaps();
	}

	@Then("^The heading on the page should be Bus Spider maps$")
	public void the_heading_on_the_page_should_be_Bus_Spider_maps() throws Throwable {
		boolean isDisplayed = mapsPage.checkHeadlineBusSpiderMapsText();
		assertTrue("Expected to see View Spider Maps text in search box", isDisplayed);
	}

	@Then("^The description on the page should be Bus Spider maps$")
	public void the_description_on_the_page_should_be_Bus_Spider_maps() throws Throwable {
		boolean isDisplayed = mapsPage.checkArticalDescriptionText();
		assertTrue("Expected to see View Spider Maps text in search box", isDisplayed);
	}

	@Then("^The search button titile on the page changed to Find Spider maps$")
	public void the_search_button_titile_on_the_page_changed_to_Find_Spider_maps() throws Throwable {
		boolean isDisplayed = mapsPage.checkFindSpiderMapsText();
		assertTrue("Expected to see View Spider Maps text in search box", isDisplayed);
	}

	@Then("^The home view titile on the page changed to Bus Spider maps$")
	public void the_home_view_titile_on_the_page_changed_to_Bus_Spider_maps() throws Throwable {
		boolean isDisplayed = mapsPage.checkHomeHeadViewText();
		assertTrue("Expected to see View Spider Maps text in search box", isDisplayed);
	}
}

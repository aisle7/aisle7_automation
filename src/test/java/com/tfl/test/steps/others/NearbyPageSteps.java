package com.tfl.test.steps.others;


import org.junit.Assert;

import com.tfl.test.driver.manager.WebDriverManager;
import com.tfl.test.pages.others.NearbyPage;
import com.tfl.test.pages.others.NearbyResultSectionMap;
import com.tfl.test.steps.CommonSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * TASK: Write all the steps necessary to drive your test scenarios to
 * completion
 * 
 * 
 */
public class NearbyPageSteps extends CommonSteps {

	private static final String NEARBY = "maps?Input=Current+location+";
	private NearbyResultSectionMap nearbyResultSectionMap;


	@Given("^I am on nearby page$")
	public void I_am_on_nearby_page() throws Throwable {
		WebDriverManager.getSpecificSection(driver, NEARBY);
		nearbyPage = new NearbyPage(driver);
	}
	
	@Given("^I am in st james park and on nearby page$")
	public void I_am_in_st_james_park_and_on_nearby_page() throws Throwable {
		WebDriverManager.getSpecificSection(driver, NEARBY);
		nearbyPage = new NearbyPage(driver);
	}

	@Then("^I should see bike icon with text \"(.*?)\" in nearby details map$")
	public void i_should_see_bike_icon_with_text_in_ssp_details_map(String text) {
		nearbyResultSectionMap = nearbyPage.getNearbyResultSectionMap();
		boolean pierIcon = nearbyResultSectionMap.checkForBikeIconOnMapPanelInfo(text);
		Assert.assertEquals("Expected to see bikepoint : " + text, true, pierIcon);
	}
	

	@When("^I select \"([^\"]*)\" from auto suggest$")
	public void I_select_from_auto_suggest(String location) throws Throwable {
		//don't share location
		//Utils.shareLocation(true);
		nearbyPage.clearSearchField();
		nearbyPage.enterSearchTerm(location);
		nearbyPage.selectFromDropDownOptions(location);
	}

}

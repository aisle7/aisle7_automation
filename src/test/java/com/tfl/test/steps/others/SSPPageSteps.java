package com.tfl.test.steps.others;


import java.util.List;

import static org.junit.Assert.*;

import com.tfl.test.driver.manager.WebDriverManager;
import com.tfl.test.pages.SSPPage;
import com.tfl.test.pages.TFLHomePage;
import com.tfl.test.steps.CommonSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * TASK: Write all the steps necessary to drive your test scenarios to
 * completion
 * 
 * 
 */
public class SSPPageSteps extends CommonSteps {

	private static final String STATIONS_STOPS_AND_PIERS_PAGE = "travel-information/stations-stops-and-piers/";


	@Given("^I am on stations stops and piers page$")
	public void I_am_on_ssp_page() throws Throwable {
		WebDriverManager.getSpecificSection(driver, STATIONS_STOPS_AND_PIERS_PAGE);
		homePage = new TFLHomePage(driver);
	}

	@Given("^I am in SSP page$")
	public void i_am_in_SSP_page() throws Throwable {
		WebDriverManager.getSpecificSection(driver, STATIONS_STOPS_AND_PIERS_PAGE);
		sspPage = new SSPPage(driver);
	}

	@When("^I search for station \"(.*?)\"$")
	public void i_search_for_station(String location) throws Throwable {
		//sspPage.searchForStation(location);
		sspPage.searchForStation(location);
		//MapUtils.waitForElementToBeVisibleByClass(driver, "stop-name", MapUtils.timeout);
		sspPage = sspPage.selectFromDropDownOptions(location);
	}

	@Then("^I should see live arrivals text in search box$")
	public void i_should_see_live_arrivals_text_in_search_box()
			throws Throwable {
		boolean isDisplayed = sspPage.checkLiveArrivalTextShownInSearchBox();
		assertTrue("Expected to see live arrivals text in search box", isDisplayed);
	}
	
	@Given("^Assuming user select Buses option from stations-stops-and-piers page$")
	public void assuming_user_select_Buses_option_from_stations_stops_and_piers_page() throws Throwable {
		WebDriverManager.getSpecificSection(driver, STATIONS_STOPS_AND_PIERS_PAGE);
		sspPage = new SSPPage(driver);
	}

	@When("^user click on bus route number from the list of which bus are you looking for\\?$")
	public void user_click_on_bus_route_number_from_the_list_of_which_bus_are_you_looking_for() throws Throwable {
		sspPage.busRouteSelect(); 
		sspPage.whichbusrouteLooking();
		sspPage.selectingbusnumber();
	}

	@Then("^user can able to see which bus stop are disrupted status in the bus stop list$")
	public void user_can_able_to_see_which_bus_stop_are_disrupted_status_in_the_bus_stop_list() throws Throwable {
		List<String> allDisruptedStops=sspPage.getAllDisrubtedStops();
		assertTrue("No Disrupted Stops Found",allDisruptedStops.size()>0);
	}
	@Given("^user in ssp page$")
	public void user_in_ssp_page() throws Throwable {
		WebDriverManager.getSpecificSection(driver, STATIONS_STOPS_AND_PIERS_PAGE);
		sspPage = new SSPPage(driver);
	}

	@When("^user select bus number from the list$")
	public void user_select_bus_number_from_the_list() throws Throwable {
		sspPage.busRouteSelect(); 
		sspPage.selectbusroute2();
		sspPage.selectbusnumber();
	}

	@Then("^user able to see disruption bus stops from the list$")
	public void user_able_to_see_disruption_bus_stops_from_the_list() throws Throwable {
		List<String> allDisruptedStops=sspPage.getAllDisrubtedStops();
		assertTrue("No Disrupted Stops Found",allDisruptedStops.size()>0);
	}
	
	@When("^user select bus number from the search option$")
	public void user_select_bus_number_from_the_search_option() throws Throwable {
		sspPage.enterBusNumber();
		sspPage.clickOnGoButton();
	}

	@When("^choose a stop form the list of bus stops$")
	public void choose_a_stop_form_the_list_of_bus_stops() throws Throwable {
	     
	}

	@Then("^user able to see text code for next bus info$")
	public void user_able_to_see_text_code_for_next_bus_info() throws Throwable {
	     
	}
	 
	@Given("^using in ssp page$")
	public void using_in_ssp_page() throws Throwable {
	     
	}

	@When("^user select specific tube for directions$")
	public void user_select_specific_tube_for_directions() throws Throwable {
	    
	}

	@Then("^user able to see view stations by and switch directions$")
	public void user_able_to_see_view_stations_by_and_switch_directions() throws Throwable {
	    
	}

	@When("^user select river bus option for journey and directions$")
	public void user_select_river_bus_option_for_journey_and_directions() throws Throwable {
	    
	}

	@Then("^user able to see view stations by and switch directions for river bus$")
	public void user_able_to_see_view_stations_by_and_switch_directions_for_river_bus() throws Throwable {
	    
	}

	@When("^user select tram link option for journey and directions$")
	public void user_select_tram_link_option_for_journey_and_directions() throws Throwable {
	    
	}

	@Then("^user able to see view stations by and switch directions for tram link$")
	public void user_able_to_see_view_stations_by_and_switch_directions_for_tram_link() throws Throwable {
	     
	}

	@When("^user select Emirates Air Line option for journey and directions$")
	public void user_select_Emirates_Air_Line_option_for_journey_and_directions() throws Throwable {
	     
	}

	@Then("^user able to see view stations by and switch directions for Emirates Air Line$")
	public void user_able_to_see_view_stations_by_and_switch_directions_for_Emirates_Air_Line() throws Throwable {
	     
	}
	
	@When("^I select \"(.*?)\" using auto suggest$")
	public void i_select_using_auto_suggest(String location) throws Throwable {
		sspPage.searchForStation(location);
		sspPage.selectFromDropDownOptions(location);
	}
	
	@Then("^I able to see BCH transport from that station$")
	public void i_able_to_see_BCH_transport_from_that_station() throws Throwable {
		sspPage.bchexpand();
		boolean isDisplayed = sspPage.bchTextValidations();
		assertTrue("Expected to see Barclays Cycle Hire text in SSP Page", isDisplayed);
		
	}
	@When("^I select Tube \"(.*?)\" using search option$")
	public void i_select_Tube_using_search_option(String location) throws Throwable {
		sspPage.searchForStation(location);
		sspPage.selectFromDropDownOptions(location);
	}

	@When("^I select as favourite of that station$")
	public void i_select_as_favourite_of_that_station() throws Throwable {
	    
	}

	@Then("^I able to see favourited station under tube option in home page$")
	public void i_able_to_see_favourited_station_under_tube_option_in_home_page() throws Throwable {
	     
	}

	@When("^I select Bus \"(.*?)\" using search option$")
	public void i_select_Bus_using_search_option(String arg1) throws Throwable {
	     
	}

	@Then("^I able to see favourited station under bus option in home page$")
	public void i_able_to_see_favourited_station_under_bus_option_in_home_page() throws Throwable {
	     
	}

	@When("^I select River \"(.*?)\" using search option$")
	public void i_select_River_using_search_option(String arg1) throws Throwable {
	     
	}

	@Then("^I able to see favourited station under river option in home page$")
	public void i_able_to_see_favourited_station_under_river_option_in_home_page() throws Throwable {
	    
	}

	@When("^I select Tram \"(.*?)\" using search option$")
	public void i_select_Tram_using_search_option(String arg1) throws Throwable {
	    
	}

	@Then("^I able to see favourited station under tram option in home page$")
	public void i_able_to_see_favourited_station_under_tram_option_in_home_page() throws Throwable {
	     
	}

	@When("^I select EAL \"(.*?)\" using search option$")
	public void i_select_EAL_using_search_option(String arg1) throws Throwable {
	     
	}

	@Then("^I able to see favourited station under EAL option in home page$")
	public void i_able_to_see_favourited_station_under_EAL_option_in_home_page() throws Throwable {
	     
	}
	
}

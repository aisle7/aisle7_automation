package com.tfl.test.steps.jp;

import static org.junit.Assert.*;

import java.util.List;

import com.tfl.test.driver.manager.WebDriverManager;
import com.tfl.test.pages.PlanAJourneyPage;
import com.tfl.test.steps.CommonSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * TASK: Write all the steps necessary to drive your test scenarios to
 * completion 
 * 
 */
public class JourneyPlannerSteps extends CommonSteps{


	@Given("^I am on Plan A Journey page$")
	public void i_am_on_Plan_A_Journey_page() throws Throwable {
		WebDriverManager.getSpecificSection(driver, PLAN_A_JOURNEY);
		journeyPage = new PlanAJourneyPage(driver);
	}
	
	
	@Then("^I should be on Plan A Journey page$")
	public void i_should_be_on_Plan_A_Journey_page() throws Throwable {
		boolean checkWeAreInJourneyPage = journeyPage.checkWeAreInJourneyPage();
		assertTrue("We are not in journey planner page", checkWeAreInJourneyPage);
	}

	@When("^I enter From \"(.*?)\" and To \"(.*?)\" and click on Plan A Journey button$")
	public void i_enter_From_and_To_and_click_on_Plan_A_Journey_button(String from, String to) throws Throwable {
		journeyPage.enterFromStation(from);
		journeyPage.selectFromDropDownOptions(from);
		journeyPage.enterToStation(to);
		journeyPage.selectFromDropDownOptions(to); 
		journeyResultsPage = journeyPage.clickPlanAJourney();
		//journeyPage.clickOnBCHJourneyType();
	}
	
	@When("^I enter From \"(.*?)\" and To \"(.*?)\" station$")
	public void i_enter_From_and_To_station(String from, String to) throws Throwable {
		journeyPage.enterFromStation(from);
		journeyPage.selectFromDropDownOptions(from);
		journeyPage.enterToStation(to);
		journeyPage.selectFromDropDownOptions(to);
		//journeyResultsPage = journeyPage.clickPlanAJourney();
	}
	
	@When("^I click plan a journey button$")
	public void i_click_plan_a_journey_button(){
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}

	@Then("^I should see the journey planner results page$")
	public void i_should_see_the_journey_planner_results_page() throws Throwable {
		assertTrue("Title not matched",journeyResultsPage.checkTitleMatches());
	}

	@When("^I select \"(.*?)\" only mode and perform a search$")
	public void i_select_only_and_perform_a_search(String mode) throws Throwable {
		//journeyResultsPage = journeyPage.clickPlanAJourney();
		//journeyPage.selectMode(mode);
		journeyPage = journeyPage.unselectallModes(mode);
		journeyPage.selectSepecifiedMode(mode);
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}
	
	@When("^I select \"(.*?)\" only option and perform a search$")
	public void i_select_only_option_and_perform_a_search(String arg1) throws Throwable { 
		journeyPage.clickAverageWalkOption();
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}
	
	@When("^I select walking only option and perform a search$")
	public void i_select_walking_only_option_and_perform_a_search() throws Throwable {
		journeyPage.clickAverageWalkOption();
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}

	@When("^I select cycle-hire only mode and perform a search$")
	public void i_select_cycle_hire_only_mode_and_perform_a_search() throws Throwable {
		journeyPage.clickCycleHireOption();
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}

	@When("^I select cycle only mode and perform a search$")
	public void i_select_cycle_only_mode_and_perform_a_search() throws Throwable {
		journeyPage.clickCycleAllTheWay();
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}

	@When("^I select fast walking only option and perform a search$")
	public void i_select_fast_walking_only_option_and_perform_a_search() throws Throwable {
		journeyPage.clickFastWalking();
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}

	@When("^I select slow walking only option and perform a search$")
	public void i_select_slow_walking_only_option_and_perform_a_search() throws Throwable {
		journeyPage.clickSlowWalking();
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}

	@When("^I select take my cycle on PT mode and perform a search$")
	public void i_select_take_my_cycle_on_PT_mode_and_perform_a_search() throws Throwable {
		journeyPage.clickCycleTakeOnTransport();
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}

	@When("^I select cycle LeaveAtStation mode and perform a search$")
	public void i_select_cycle_LeaveAtStation_mode_and_perform_a_search() throws Throwable {
		journeyPage.clickCycleLeaveAtStation();
		journeyResultsPage = journeyPage.clickPlanAJourney();    
	}
 
	@When("^I select show me routes with fewest changes and perform a search$")
	public void i_select_show_me_routes_with_fewest_changes_and_perform_a_search() throws Throwable {
		journeyPage.clickShowMeleastinterchange();
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}

	@Then("^I should see routes with fewest changes journey option$")
	public void i_should_see_routes_with_fewest_changes_journey_option() throws Throwable {
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size());  
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",2,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",5,allJPList3.size()); 
	}

	@When("^I select Showing routes with least walking and perform a search$")
	public void i_select_Showing_routes_with_least_walking_and_perform_a_search() throws Throwable {
		journeyPage.clickShowMeleastwalking();
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}

	@Then("^I should see routes with least walking journey option$")
	public void i_should_see_routes_with_least_walking_journey_option() throws Throwable {
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size());  
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",7,allJPList3.size());      
	}
	
	@When("^I select \"(.*?)\" and \"(.*?)\" mode and perform a search$")
	public void i_select_and_mode_and_perform_a_search(String mode1, String mode2) throws Throwable {
		journeyPage.selectModes(mode1,mode2);
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}
	
	@When("^I select \"(.*?)\",\"(.*?)\",\"(.*?)\" and \"(.*?)\" modes and perform a search$")
	public void i_select_and_modes_and_perform_a_search(String mode1, String mode2, String mode3, String mode4) throws Throwable {
		journeyPage.selectforModes(mode1,mode2,mode3,mode4);
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}
	
	@When("^I select \"(.*?)\" and \"(.*?)\" modes$")
	public void i_select_and_modes(String mode1, String mode2) throws Throwable {
		journeyPage.selectModes(mode1,mode2);
	}

	@When("^I click on perform a search$")
	public void i_click_on_perform_a_search() throws Throwable {
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}
	
	@When("^I select \"(.*?)\" and \"(.*?)\" more modes$")
	public void i_select_and_more_modes(String mode1, String mode2) throws Throwable {
		journeyPage.selectSepecifiedModes(mode1,mode2);
	}
	
	@When("^Select weekend time$")
	public void select_weekend_time() throws Throwable {
		journeyPage.selectWeekendTime();
		
	}
	
	@Then("^I should see journeys with multiple disruptions$")
	public void i_should_see_journeys_with_multiple_disruptions() throws Throwable {
		boolean isDisplayed = journeyResultsPage.checkMultipleDisruptionsDisplayed();
		assertTrue("Expected to see multiple bus disruptions",isDisplayed);
	}
	@When("^I select only cycle mode and perform a search$")
	public void i_select_only_cycle_mode_and_perform_a_search() throws Throwable {
		journeyPage.selectCycleMode();
	}

	@When("^I enter From \"(.*?)\" and To \"(.*?)\" stations$")
	public void i_enter_From_and_To_stations(String from, String to) throws Throwable {
		journeyPage.enterFromStation(from);
		journeyPage.enterToStation(to);
		journeyPage.clickPlanMyJourney();
	}

	@Then("^I able to see Easy cycle journey options$")
	public void i_able_to_see_Easy_cycle_journey_options() throws Throwable {
		boolean isDisplayed = journeyPage.checkEasyCycleText();
		assertTrue("Expected to see Easy text", isDisplayed);  
	}

	@Then("^I able to see Moderate cycle journey options$")
	public void i_able_to_see_Moderate_cycle_journey_options() throws Throwable {
		boolean isDisplayed = journeyPage.checkModerateCycleText();
		assertTrue("Expected to see Moderate text", isDisplayed);  
	}

	@Then("^I able to see Fast cycle journey options$")
	public void i_able_to_see_Fast_cycle_journey_options() throws Throwable {
		boolean isDisplayed = journeyPage.checkFastCycleText();
		assertTrue("Expected to see Fast text", isDisplayed);  
	} 
	@Then("^I should see View Details$")
	public void i_should_see_View_Details() throws Throwable {
		journeyPage.clickEasyViewDetails();
		boolean isDisplayed = journeyPage.verifyBCHAvailability();
		assertTrue("Expected to see bikes available text", isDisplayed);
		//boolean isDisplayed1 = journeyPage.verifyBCHSpaces();
		//assertTrue("Expected to see spaces text", isDisplayed1);
		//journeyPage.clickModerateViewDetails();
		//boolean isDisplayed2 = journeyPage.verifyBCHAvailability();
		//assertTrue("Expected to see bikes available text", isDisplayed2);
		//boolean isDisplayed3 = journeyPage.verifyBCHSpaces();
		//assertTrue("Expected to see spaces text", isDisplayed3);
		//journeyPage.clickFastViewDetails();
		//boolean isDisplayed4 = journeyPage.verifyBCHAvailability();
		//assertTrue("Expected to see bikes available text", isDisplayed4);
		//boolean isDisplayed5 = journeyPage.verifyBCHSpaces();
		//assertTrue("Expected to see spaces text", isDisplayed5);
	}
	
	@Given("^I am on Plan A Journey new page$")
	public void i_am_on_Plan_A_Journey_new_page() throws Throwable {
		WebDriverManager.getSpecificSection(driver, PLAN_A_JOURNEY);
		journeyPage = new PlanAJourneyPage(driver);
	}

	@When("^I enter From \"(.*?)\" and To \"(.*?)\" and click on Plan A Journey$")
	public void i_enter_From_and_To_and_click_on_Plan_A_Journey(String from, String to) throws Throwable {
		journeyPage.enterFromStation(from);
		journeyPage.selectFromDropDownOptions(from);
		journeyPage.enterToStation(to);
		journeyPage.selectFromDropDownOptions(to); 
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}

	@Then("^I should see the fatest journey option$")
	public void i_should_see_the_fatest_journey_option() throws Throwable {
		
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size());  
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",2,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",6,allJPList3.size()); 
	}
	
	@Then("^I should see the array of JP options without walking only$")
	public void i_should_see_the_array_of_JP_options_without_walking_only() throws Throwable {
		//List<String> allJPList=journeyPage.cycleRouteDetailsTest(); 
		//assertEquals(3,allJPList.size());  
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size());  
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",1,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",5,allJPList3.size()); 
		 
	}

	@Then("^I should see the array of JP options without walking and cycle$")
	public void i_should_see_the_array_of_JP_options_without_walking_and_cycle() throws Throwable {
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertTrue("No Disrupted Stops Found",allJPList.size()>0); 
		
		List<String> allJPList2=journeyPage.JpListofTimeBoxesTest(); 
		assertTrue("No Disrupted Stops Found",allJPList2.size()>0); 
		
		List<String> allJPList3=journeyPage.JpListofViewDetailsTest(); 
		assertTrue("No Disrupted Stops Found",allJPList3.size()>0);
		
		List<String> allJPList4=journeyPage.ListofAllOtherJpModesTest(); 
		assertTrue("No Disrupted Stops Found",allJPList4.size()>0);
	}

	@Then("^I should see JP result without walking,cycle and bus option$")
	public void i_should_see_JP_result_without_walking_cycle_and_bus_option() throws Throwable {
		
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertTrue("No Disrupted Stops Found",allJPList.size()>0); 
		
		List<String> allJPList2=journeyPage.JpListofTimeBoxesTest(); 
		assertTrue("No Disrupted Stops Found",allJPList2.size()>0); 
		
		List<String> allJPList3=journeyPage.JpListofViewDetailsTest(); 
		assertTrue("No Disrupted Stops Found",allJPList3.size()>0);
		
		List<String> allJPList4=journeyPage.ListofAllOtherJpModesTest(); 
		assertTrue("No Disrupted Stops Found",allJPList4.size()>0);
	}

	@Then("^I should see the array of JP options$")
	public void i_should_see_the_array_of_JP_options() throws Throwable {
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertTrue("No Disrupted Stops Found",allJPList.size()>0); 
		
		List<String> allJPList2=journeyPage.JpListofTimeBoxesTest(); 
		assertTrue("No Disrupted Stops Found",allJPList2.size()>0); 
		
		List<String> allJPList3=journeyPage.JpListofViewDetailsTest(); 
		assertTrue("No Disrupted Stops Found",allJPList3.size()>0);
		
		List<String> allJPList4=journeyPage.ListofAllOtherJpModesTest(); 
		assertTrue("No Disrupted Stops Found",allJPList4.size()>0);
	}

	@Then("^I should see the dockstation journey result$")
	public void i_should_see_the_dockstation_journey_result() throws Throwable {
		//List<String> allJPList=journeyPage.cycleRouteDetailsTest(); 
		//assertEquals(3,allJPList.size()); 
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size());  
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",2,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",5,allJPList3.size()); 
	}
 
	@Then("^I should see the option type and edit button$")
	public void i_should_see_the_option_type_and_edit_button() throws Throwable {
		boolean checkEditButtonText = journeyPage.editButtonText();
		assertTrue("Edit button", checkEditButtonText);
		//boolean moreOptionsAccessibilityText = journeyPage.moreOptionsAccessibility();
		//assertTrue("more options button", moreOptionsAccessibilityText);
	}

	@Then("^mode of transport per journey leg involved$")
	public void mode_of_transport_per_journey_leg_involved() throws Throwable {
		List<String> allJPList=journeyPage.TubeiconDisplayedTest();  
		assertTrue("No Disrupted Stops Found",allJPList.size()>0);
		List<String> allJPList1=journeyPage.departsArrivesDisplayedTest();  
		assertTrue("No Disrupted Stops Found",allJPList1.size()>0);
	
	}

	@Then("^route for mode of transport$")
	public void route_for_mode_of_transport() throws Throwable {
		List<String> allJPList=journeyPage.journeyTimeMinsTest();  
		assertTrue("No Disrupted Stops Found",allJPList.size()>0);
		List<String> allJPList1=journeyPage.walkingIconDisplayTest();  
		assertTrue("No Disrupted Stops Found",allJPList1.size()>0);
	}

	@Then("^journey time in minutes and hours$")
	public void journey_time_in_minutes_and_hours() throws Throwable {
		List<String> allJPList=journeyPage.cycleIconDisplayTest();  
		assertTrue("No Disrupted Stops Found",allJPList.size()>0);
		List<String> allJPList1=journeyPage.busIconDisplayTest();  
		assertTrue("No Disrupted Stops Found",allJPList1.size()>0);
	}

	@Then("^disruption notification for journey leg if applicable$")
	public void disruption_notification_for_journey_leg_if_applicable() throws Throwable {
		//List<String> allJPList=journeyPage.timeIconDisplayTest();  
		//assertTrue("No Time Icon Display",allJPList.size()>0);
		List<String> allJPList1=journeyPage.disruptionsIconDisplayTest();  
		assertTrue("No Disrupted Stops Icon Found",allJPList1.size()>0);
	}
	@Then("^I should see the tube journey option$")
	public void i_should_see_the_tube_journey_option() throws Throwable { 
		
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size());  
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",2,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",6,allJPList3.size()); 
		
	}

	@Then("^I should see the dlr journey option$")
	public void i_should_see_the_dlr_journey_option() throws Throwable { 
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size());  
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",2,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",6,allJPList3.size()); 
		 
	}

	@Then("^I should see the bch journey option$")
	public void i_should_see_the_bch_journey_option() throws Throwable { 
		
		List<String> allJPList=journeyPage.cycleRouteDetailsTest(); 
		assertEquals(3,allJPList.size());  
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",7,allJPList3.size()); 
	}

	@Then("^I should see the cycle journey option$")
	public void i_should_see_the_cycle_journey_option() throws Throwable {
		List<String> allJPList=journeyPage.cycleRouteDetailsTest();
		assertEquals(3,allJPList.size());   
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",7,allJPList3.size()); 
	}
	@When("^I Unselect \"(.*?)\" only mode and perform a search$")
	public void i_Unselect_only_mode_and_perform_a_search(String mode) throws Throwable {
		journeyPage.unselectMode(mode);
		journeyResultsPage = journeyPage.clickPlanAJourney();
	}

	@Then("^I should see the journey options$")
	public void i_should_see_the_journey_options() throws Throwable {
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertTrue("No Disrupted Stops Found",allJPList.size()>0); 
		
		List<String> allJPList2=journeyPage.JpListofTimeBoxesTest(); 
		assertTrue("No Disrupted Stops Found",allJPList2.size()>0); 
		
		List<String> allJPList3=journeyPage.JpListofViewDetailsTest(); 
		assertTrue("No Disrupted Stops Found",allJPList3.size()>0);
	}
	@Then("^I should see the cycle journey options list$")
	public void i_should_see_the_cycle_journey_options_list() throws Throwable {
		List<String> allJPList=journeyPage.cycleRouteDetailsTest(); 
		assertTrue("No Disrupted Stops Found",allJPList.size()>0); 
	}
	@Then("^I should see the total cycle journey distance$")
	public void i_should_see_the_total_cycle_journey_distance() throws Throwable {
		List<String> allJPList=journeyPage.totalDistanceText(); 
		assertTrue("No Disrupted Stops Found",allJPList.size()>0);
	}

	@Then("^I should see the total walking journey distance$")
	public void i_should_see_the_total_walking_journey_distance() throws Throwable {
		List<String> allJPList=journeyPage.totalWalkingDistanceText(); 
		assertTrue("No Disrupted Stops Found",allJPList.size()>0);
	}
	@When("^I click on suspended docking stations view details$")
	public void i_click_on_suspended_docking_stations_view_details() throws Throwable {
		journeyPage.suspendedDockstationButton();
	}

	@Then("^I should see suspended and relocated docking stations list$")
	public void i_should_see_suspended_and_relocated_docking_stations_list() throws Throwable {
		List<String> allJPList=journeyPage.suspendedDockstationList(); 
		assertTrue("No Disrupted Stops Found",allJPList.size()>0);
	}
	@Then("^I should see the bus journey options$")
	public void i_should_see_the_bus_journey_options() throws Throwable {  
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size());  
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",2,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",6,allJPList3.size()); 
	}
	@Then("^I should see the river-bus journey option$")
	public void i_should_see_the_river_bus_journey_option() throws Throwable { 
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size());  
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",2,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",6,allJPList3.size()); 
	}
	@Then("^I should see the tram journey option$")
	public void i_should_see_the_tram_journey_option() throws Throwable {
		 
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size());  
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",2,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",6,allJPList3.size()); 
	}
	
	@Then("^I should see the cable car journey option$")
	public void i_should_see_the_cable_car_journey_option() throws Throwable { 
		
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals("Fatest JP Display",3,allJPList.size());  
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",2,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",6,allJPList3.size()); 
	}

	@Then("^I should see the overground journey option$")
	public void i_should_see_the_overground_journey_option() throws Throwable {
		List<String> allJPList4=journeyPage.busIconDisplayTest(); 
		assertTrue("No Disrupted Stops Found",allJPList4.size()>0); 
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size());  
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",1,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",6,allJPList3.size()); 
	}

	@Then("^I should see the national rail journey option$")
	public void i_should_see_the_national_rail_journey_option() throws Throwable { 
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size()); 
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",8,allJPList3.size()); 
	}

	@Then("^I should see the coach journey option$")
	public void i_should_see_the_coach_journey_option() throws Throwable {
		List<String> allJPList4=journeyPage.busIconDisplayTest(); 
		assertTrue("No Disrupted Stops Found",allJPList4.size()>0); 
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size());  
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",2,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",6,allJPList3.size()); 
	}

	@Then("^I should see the walking journey option$")
	public void i_should_see_the_walking_journey_option() throws Throwable { 
		List<String> allJPList=journeyPage.JpWalkingDisplayedTest(); 
		assertEquals(1,allJPList.size());  
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",7,allJPList3.size()); 
	}
	@When("^I click on bus only link from result page$")
	public void i_click_on_bus_only_link_from_result_page() throws Throwable {
		journeyPage.busOnlyOptionLinkTest();
	}

	@Then("^I should see the bus only result with other options$")
	public void i_should_see_the_bus_only_result_with_other_options() throws Throwable {
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size());
		List<String> allJPList1=journeyPage.busIconDisplayTest();  
		assertTrue("Bus Icon Display",allJPList1.size()>0);
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",2,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",5,allJPList3.size());
	}
	@When("^I click on Cycle hire link from result page$")
	public void i_click_on_Cycle_hire_link_from_result_page() throws Throwable {
		journeyPage.cycleHireOptionLinkTest();
	}

	@Then("^I should see the Cycle hire result with other options$")
	public void i_should_see_the_Cycle_hire_result_with_other_options() throws Throwable { 
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",7,allJPList3.size());
	}

	@When("^I click on Least Walking link from result page$")
	public void i_click_on_Least_Walking_link_from_result_page() throws Throwable {
		journeyPage.leastWalkingOptionLinkTest();
	}

	@Then("^I should see the Least Walking result with other options$")
	public void i_should_see_the_Least_Walking_result_with_other_options() throws Throwable { 
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",7,allJPList3.size());
	}

	@When("^I click on Fewest Changes link from result page$")
	public void i_click_on_Fewest_Changes_link_from_result_page() throws Throwable {
		journeyPage.fewestChangesOptionLinkTest();
	}

	@Then("^I should see the Fewest Changes result with other options$")
	public void i_should_see_the_Fewest_Changes_result_with_other_options() throws Throwable {
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",2,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",5,allJPList3.size());
	}

	@When("^I click on Full step free access route link from result page$")
	public void i_click_on_Full_step_free_access_route_link_from_result_page() throws Throwable {
		journeyPage.fullSTepFreeAccessRouteOptionLinkTest();
	}

	@Then("^I should see the Full step free access route result with other options$")
	public void i_should_see_the_Full_step_free_access_route_result_with_other_options() throws Throwable {
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",2,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",5,allJPList3.size());
	}

	@When("^I click on Full Cycling link from result page$")
	public void i_click_on_Full_Cycling_link_from_result_page() throws Throwable {
		journeyPage.cyclingOptionLinkTest();
	}

	@Then("^I should see the Cycling result with other options$")
	public void i_should_see_the_Cycling_result_with_other_options() throws Throwable { 
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",7,allJPList3.size());
	}

	@When("^I click on Full Walking link from result page$")
	public void i_click_on_Full_Walking_link_from_result_page() throws Throwable {
		journeyPage.walkingOnlyOptionLinkTest();
	}

	@Then("^I should see the Walking result with other options$")
	public void i_should_see_the_Walking_result_with_other_options() throws Throwable { 
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",7,allJPList3.size());
	}
	@Then("^I should see the Fastest jp result with other options$")
	public void i_should_see_the_Fastest_jp_result_with_other_options() throws Throwable {
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size()); 
		List<String> allJPList2=journeyPage.ListofCycleAndWalkingJPTest(); 
		assertEquals("CycleAndWalking",2,allJPList2.size());
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",5,allJPList3.size());
	}

	@Then("^I should see the narional rail journey option$")
	public void i_should_see_the_narional_rail_journey_option() throws Throwable {
		List<String> allJPList=journeyPage.JpTransportDisplayedTest(); 
		assertEquals(3,allJPList.size()); 
		List<String> allJPList3=journeyPage.ListofAllOtherJpModesTest(); 
		assertEquals("OtherJourneyOptions",5,allJPList3.size());
	}
	
//Aisle7 Stories 
	@Given("^I am on formulation manager login screen$")
	public void i_am_on_formulation_manager_login_screen() throws Throwable {
		WebDriverManager.getSpecificSection(driver, PLAN_A_JOURNEY);
		journeyPage = new PlanAJourneyPage(driver);
	}

	@When("^I enter LoginID \"([^\"]*)\" and Password \"([^\"]*)\" and click on Login button$")
	public void i_enter_LoginID_and_Password_and_click_on_Login_button(String login, String password) throws Throwable {
		journeyPage.loginToMainScreen(login); 
		journeyPage.passwordToMainScreen(password); 
		journeyPage.clickOnLoginButton();
	}

	@Then("^I able see the formulation manager home screen$")
	public void i_able_see_the_formulation_manager_home_screen() throws Throwable {
		boolean checkHomeScreenText = journeyPage.homeScreenText();
		assertTrue("FORMULATION MANAGER", checkHomeScreenText);
	}


@When("^click on ingredient research link$")
public void click_on_ingredient_research_link() throws Throwable {
   journeyPage.clickIngredientResearchLink();

}

@Then("^I able see the ingredient research home screen$")
public void i_able_see_the_ingredient_research_home_screen() throws Throwable {
     
}


}

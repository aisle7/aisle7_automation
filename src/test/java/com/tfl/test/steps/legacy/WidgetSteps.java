package com.tfl.test.steps.legacy;
import org.junit.Assert;

import com.tfl.test.driver.manager.WebDriverManager;
import com.tfl.test.pages.WidgetsPage;
import com.tfl.test.steps.CommonSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by balajiravivarman on 14/10/2014.
 */
public class WidgetSteps extends CommonSteps{

    public static final String WIDGETS_PAGE = "forms/12425.aspx";
    private WidgetsPage widgetsPage;

    @Given("^User at the Widgets Landing Page$")
    public void User_at_the_Widgets_Landing_Page() throws Throwable {
        //assertTrue("We are not in cip landing page", widgetsPage.checkWeAreTrackWidgetsPage());
    	WebDriverManager.getSpecificSection(driver, WIDGETS_PAGE);
    	widgetsPage = new WidgetsPage(driver);
    }

    @When("^User clicks to add JP Widget$")
    public void User_clicks_to_add_JP_Widget() throws Throwable {
        widgetsPage.addJPWidget();
    }
    
    @Then("^User is presented with the JP Widget landing page$")
    public void user_is_presented_with_the_JP_Widget_landing_page() throws Throwable {
        boolean trackWidgetsPage = widgetsPage.checkWeAreTrackWidgetsPage();
        Assert.assertTrue("Expected to be in JP Widget landing page", trackWidgetsPage);
    }
    @When("^User look for live travel news for roads$")
    public void user_look_for_live_travel_news_for_roads() throws Throwable {
    	widgetsPage.checkWidgetsText();
    }

    @Then("^User can able to see Roads widget option$")
    public void user_can_able_to_see_Roads_widget_option() throws Throwable {
    	boolean isDisplayed = widgetsPage.checkRoadsText();
        Assert.assertTrue("Expected to be in Roads Widget text", isDisplayed);
    }
}

package com.tfl.test.steps.fares;

import static junit.framework.TestCase.assertTrue;

import org.junit.Assert;

import com.tfl.test.driver.manager.WebDriverManager;
import com.tfl.test.pages.FaresAndPaymentPage;
import com.tfl.test.steps.CommonSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by balajiravivarman on 09/10/2014.
 */
public class FaresFinderSteps extends CommonSteps{

    public static final String FARE_FINDER = "fares-and-payments/fares/single-fare-finder";
    private FaresAndPaymentPage faresAndPaymentPage;


    @Given("^\"([^\"]*)\" is at the Fares And Payments Page$")
    public void is_at_the_Fares_And_Payments_Page(String arg1) throws Throwable {
        assertTrue(Boolean.TRUE);
    }
    
    @Given("^I am at Fares And Payments page$")
    public void i_am_at_fares_and_payments_page(){
      WebDriverManager.getSpecificSection(driver, FARE_FINDER);
      faresAndPaymentPage = new FaresAndPaymentPage(driver);
    }

    @Given("^\"([^\"]*)\" is at the single fare finder page$")
    public void is_at_the_single_fare_finder_page(String arg1) throws Throwable {
        boolean singleFinderPage = faresAndPaymentPage.checkWeAreAtSFFPage();
        Assert.assertTrue("We are not in single fare finder page", singleFinderPage);
    }

    @When("^he check fares between \"([^\"]*)\" and \"([^\"]*)\"$")
    public void he_enters_from_station_and_to_station(String from, String to) throws Throwable {
        faresAndPaymentPage.findFare(from,to);
    }

    @Then("^he is provided with single fares$")
    public void he_is_provided_with_single_fares() throws Throwable {
        assertTrue(Boolean.TRUE);
    }
}

package com.tfl.test.steps.status;

import static org.junit.Assert.assertTrue;

import java.util.List;

import com.tfl.test.domain.Line;
import com.tfl.test.driver.manager.WebDriverManager;
import com.tfl.test.pages.StatusUpdatesPage;
import com.tfl.test.steps.CommonSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by balajiravivarman on 08/10/2014.
 */
public class StatusUpdateSteps extends CommonSteps{

    public static final String TRACK_STATUS = "tube-dlr-overground/status/";
    private StatusUpdatesPage statusUpdatesPage;
	private Line line;

    
    @Given("^User at the Status Updates Page$")
    public void User_at_the_Status_Updates_Page() throws Throwable {
    	WebDriverManager.getSpecificSection(driver, TRACK_STATUS);
        statusUpdatesPage = new StatusUpdatesPage(driver);
    }

	@Given("^User is in \"(.*?)\" status updates page$")
	public void user_is_in_status_updates_page(String mode) throws Throwable {
    	WebDriverManager.getSpecificSection(driver, TRACK_STATUS);
        statusUpdatesPage = new StatusUpdatesPage(driver);
        statusUpdatesPage.selectMode(mode);
	}
	
	

	@When("^User expands a random disruption for \"(.*?)\"$")
	public void user_expands_a_random_disruption(String mode) throws Throwable {
		line = statusUpdatesPage.expandARandomDisruption(mode);
	}

    @When("^User expands a line \"(.*?)\" for mode \"(.*?)\"$")
    public void user_expands_a_line(String lineName, String mode) throws Throwable {
		line = statusUpdatesPage.expandALineDisruption(lineName, mode);
    }

    @Then("^User see correct status message for \"(.*?)\"$")
    public void user_see_correct_status_message_for(String lineName) throws Throwable {
    	if(line!=null && line.getName().equals(lineName)){
			boolean matches = statusUpdatesPage.checkAPIDataMatchesWithDisplayedStatusSeverity(line);
			assertTrue("Status severity text didn't match", matches);
    	}else{
			assertTrue("Problem expanding line : " + lineName, false);
    	}
    }

	
	@Then("^User see correct disruption message$")
	public void user_see_correct_disruption_message() throws Throwable {
		boolean matches = statusUpdatesPage.checkAPIDataMatchesWithDisplayedDisruptionMessage(line);
		assertTrue("Disruptions messages didn't match with API", matches);
	}
	
	@Then("^User see correct status message$")
	public void user_see_correct_status_message() throws Throwable {
		boolean matches = statusUpdatesPage.checkAPIDataMatchesWithDisplayedStatusSeverity(line);
		assertTrue("Status severity text didn't match", matches);
	}

	@Then("^User should see \"(.*?)\" map$")
	public void user_should_see_tram_map(String mapType) throws Throwable {
		boolean isCorrectMapDisplayed = statusUpdatesPage.checkCorrectMapIsDsiplayed(mapType);
		assertTrue("Expected to see map for : " + mapType, isCorrectMapDisplayed);
	}


	@When("^User selects \"(.*?)\" from calendar$")
	public void user_selects_from_calendar(String dateText) throws Throwable {
		statusUpdatesPage = statusUpdatesPage.selectDate(dateText);
	}
	
	

    @Then("^User is presented with the current line status$")
    public void User_is_presented_with_the_current_line_status() throws Throwable {
        List<Line> lineList = statusUpdatesPage.getLineStatusDTO(driver);
        assertTrue(statusUpdatesPage.checkThisLineIsDisplayed("Bakerloo", lineList));
        assertTrue(statusUpdatesPage.checkThisLineIsDisplayed("Central", lineList));
        assertTrue(statusUpdatesPage.checkThisLineIsDisplayed("Circle", lineList));
        assertTrue(statusUpdatesPage.checkThisLineIsDisplayed("District", lineList));
        assertTrue(statusUpdatesPage.checkThisLineIsDisplayed("DLR", lineList));
        assertTrue(statusUpdatesPage.checkThisLineIsDisplayed("Hammersmith & City", lineList));
        assertTrue(statusUpdatesPage.checkThisLineIsDisplayed("Jubilee", lineList));
        assertTrue(statusUpdatesPage.checkThisLineIsDisplayed("Metropolitan", lineList));
        assertTrue(statusUpdatesPage.checkThisLineIsDisplayed("Northern", lineList));
        assertTrue(statusUpdatesPage.checkThisLineIsDisplayed("London Overground", lineList));
        assertTrue(statusUpdatesPage.checkThisLineIsDisplayed("Piccadilly", lineList));
        assertTrue(statusUpdatesPage.checkThisLineIsDisplayed("Victoria", lineList));
        assertTrue(statusUpdatesPage.checkThisLineIsDisplayed("Waterloo & City", lineList));

    }

    @When("^the user selects the \"([^\"]*)\" from the list of modes$")
    public void the_user_selects_the_from_the_list_of_modes(String mode) throws Throwable {
        assertTrue(Boolean.TRUE);
    }

    @Then("^the user is presented with the road status page$")
    public void the_user_is_presented_with_the_road_status_page() throws Throwable {
        assertTrue(Boolean.TRUE);
    }
    
     
    @Then("^Users should see \"(.*?)\" map$")
    public void users_should_see_map(String mapType) throws Throwable {
    	List<Line> lineList = statusUpdatesPage.getLineStatusDTO(driver);
        assertTrue(statusUpdatesPage.checkThisLineIsDisplayed("Bakerloo", lineList));
        assertTrue(statusUpdatesPage.checkThisLineIsDisplayed("Central", lineList));
        boolean isCorrectMapDisplayed = statusUpdatesPage.checkCorrectTubeIsDsiplayed(mapType);
		assertTrue("Expected to see map for : " + mapType, isCorrectMapDisplayed);
    }
    
    @Given("^user in Status Updates page$")
    public void user_in_Status_Updates_page() throws Throwable {
    	WebDriverManager.getSpecificSection(driver, TRACK_STATUS);
        statusUpdatesPage = new StatusUpdatesPage(driver);
    }

    @When("^user select \"(.*?)\" option from modes-dropdown-placeholder$")
    public void user_select_option_from_modes_dropdown_placeholder(String mode) throws Throwable {
    	statusUpdatesPage.selectMode(mode);
    }

    @When("^user select bus number to find a bus stop or route$")
    public void user_select_bus_number_to_find_a_bus_stop_or_route() throws Throwable {
    	statusUpdatesPage.enterBusInput();
    	statusUpdatesPage.goButtonClick();
    	statusUpdatesPage.option2fromBusInput();
    }

    @When("^user select \"(.*?)\" form current date$")
    public void user_select_form_current_date(String dateText) throws Throwable {
    	statusUpdatesPage = statusUpdatesPage.selectDate(dateText);
    }

    @Then("^user able to see disruption bus stops$")
    public void user_able_to_see_disruption_bus_stops() throws Throwable {
    	boolean matches = statusUpdatesPage.checkAPIDataMatchesWithDisplayedDisruptionMessage(line);
		assertTrue("Disruptions messages didn't match with API", matches);
    }

}

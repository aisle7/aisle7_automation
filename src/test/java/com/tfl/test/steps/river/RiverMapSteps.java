package com.tfl.test.steps.river;

import org.junit.Assert;

import com.tfl.test.driver.manager.WebDriverManager;
import com.tfl.test.pages.map.river.RiverBusPage;
import com.tfl.test.pages.map.river.RiverPage;
import com.tfl.test.pages.map.river.RiverToursPage;
import com.tfl.test.steps.CommonSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * TASK: Write all the steps necessary to drive your test scenarios to
 * completion
 * 
 */
public class RiverMapSteps extends CommonSteps {

	public static final String RIVER_PAGE = "maps/river";
	public static final String RIVER_TOURS_PAGE = "maps/river/river-tours";
	public static final String RIVER_BUS_PAGE = "maps/river/river-bus";

	@Given("^User is on river map page$")
	public void user_is_on_river_map_page() throws Throwable {
		WebDriverManager.getSpecificSection(driver, RIVER_PAGE);
		riverPage = new RiverPage(driver);
	}

	@Given("^User is on river tours map page$")
	public void user_is_on_river_tours_map_page() throws Throwable {
		WebDriverManager.getSpecificSection(driver, RIVER_TOURS_PAGE);
		riverToursPage = new RiverToursPage(driver);
	}

	@Given("^User is on river bus map page$")
	public void user_is_on_river_bus_map_page() throws Throwable {
		WebDriverManager.getSpecificSection(driver, RIVER_BUS_PAGE);
		riverBusPage = new RiverBusPage(driver);
	}
	

	@Then("^Check page title contains \"(.*?)\"$")
	public void check_page_title_contains(String text) throws Throwable {
		String title = getPageTitle();
		Assert.assertTrue("Title should contain : " + text, title.contains(text));
	}


	@When("^They click zoom in on river tours map$")
	public void they_click_zoom_in_on_river_tours_map() throws Throwable {
		mapSection = riverToursPage.getMapSection();
		mapSection.clickZoomIn();
	}

	@When("^They click zoom out on river tours map$")
	public void they_click_zoom_out_on_river_tours_map() throws Throwable {
		mapSection = riverToursPage.getMapSection();
		mapSection.clickZoomOut();
	}
	
	@When("^They click zoom in on river bus map$")
	public void they_click_zoom_in_on_river_bus_map() throws Throwable {
		mapSection = riverBusPage.getMapSection();
		mapSection.clickZoomIn();
	}
	
	@When("^They click zoom out on river bus map$")
	public void they_click_zoom_out_on_river_bus_map() throws Throwable {
		mapSection = riverBusPage.getMapSection();
		mapSection.clickZoomOut();
	}
	
	@Then("^Check map zoom in level changed$")
	public void check_map_zoom_in_level_changes() throws Throwable {
		boolean increased = mapSection.checkZoomLevelHaveIncreased();
		Assert.assertTrue("Zoom level should have changed", increased);
	}

	@Then("^Check map zoom out level changed$")
	public void check_map_zoom_out_level_changes() throws Throwable {
		boolean increased = mapSection.checkZoomLevelHaveDecreased();
		Assert.assertTrue("Zoom level should have changed", increased);
	}


	@When("^They perform full zoom in on river tours map$")
	public void they_perform_full_zoom_in_on_river_tours_map() throws Throwable {
		mapSection = riverToursPage.getMapSection();
		mapSection.fullZoomIn();
	}
	
	@When("^They perform full zoom in on river bus map$")
	public void they_perform_full_zoom_in_on_river_bus_map() throws Throwable {
		mapSection = riverBusPage.getMapSection();
		mapSection.fullZoomIn();
	}
	
	@Then("^Check map zoom level is \"(.*?)\" or more$")
	public void check_map_zoom_level_is_or_more(String zoomLevel) throws Throwable {
		boolean zoomLevelMatched = mapSection.checkCurrentZoomLevelIsAsExpected(zoomLevel);
		Assert.assertTrue("Map fully zoomed in", zoomLevelMatched);
		
	}
	
	@When("^They perform full zoom out on river tours map$")
	public void they_perform_full_zoom_out_on_river_tours_map() throws Throwable {
		mapSection = riverToursPage.getMapSection();
		mapSection.fullZoomOut();
	}
	
	@When("^They perform full zoom out on river bus map$")
	public void they_perform_full_zoom_out_on_river_bus_map() throws Throwable {
		mapSection = riverBusPage.getMapSection();
		mapSection.fullZoomOut();
	}

	@Then("^Check map zoom level is less than \"(.*?)\"$")
	public void check_map_zoom_level_is_less_than(String zoomLevel) throws Throwable {
		boolean zoomLevelMatched = mapSection.checkCurrentZoomOutLevelIsAround(zoomLevel);
		Assert.assertTrue("Map fully zoomed out", zoomLevelMatched);
	}

}

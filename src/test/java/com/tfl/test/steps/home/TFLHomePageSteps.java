package com.tfl.test.steps.home;

import org.junit.Assert;

import com.tfl.test.driver.manager.WebDriverManager;
import com.tfl.test.pages.JourneyPlannerResultsPage;
import com.tfl.test.pages.TFLHomePage;
import com.tfl.test.steps.CommonSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * TASK: Write all the steps necessary to drive your test scenarios to
 * completion
 * 
 * 
 */
public class TFLHomePageSteps extends CommonSteps {

	@Given("^I am in tfl home page$")
	public void i_am_in_tfl_home_page() throws Throwable {
		WebDriverManager.getSpecificSection(driver, "");
		homePage = new TFLHomePage(driver);

	}

	@When("^I search for bus route from \"(.*?)\" and to \"(.*?)\"$")
	public void i_search_for_bus_route_from_and_to(String from, String to)
			throws Throwable {
		homePage.enterFromStation(from);
		homePage.enterToStation(to);
		// homePage.selectBusOnlyMode();
		homePage.clickPlanAJourney();
	}

	@Then("^I should see bus disruptions displayed in disruption box$")
	public void i_should_see_bus_disruptions_displayed_in_disruption_box()
			throws Throwable {
		journeyResultsPage = new JourneyPlannerResultsPage(driver);
		boolean disruptionsDisplayed = journeyResultsPage.checkMultipleDisruptionsDisplayed();
		Assert.assertTrue("Expected to see some disruptions as part of JP result", disruptionsDisplayed);
	}

	@Then("^I should see live arrivals text$")
	public void i_should_see_live_arrivals_text() throws Throwable {
		boolean isDisplayed = homePage.checkLiveArrivalsLinksIsDisplayed();
		Assert.assertTrue("Expected to see live arrivals link", isDisplayed);
	}

}

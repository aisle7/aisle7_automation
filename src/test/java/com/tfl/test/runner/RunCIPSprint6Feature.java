package com.tfl.test.runner;
import org.junit.Ignore;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * CHANGE : report name to something else or it may overwrite existing ones with same name
 */
@RunWith(Cucumber.class)
@CucumberOptions(format = { "pretty", "html:target/cip-sprint1-html-report","json:target/report_cip_sprint1.json" },
        glue = { "com.tfl.test.steps" },
        features = { "classpath:com/tfl/bdd/test/cip/"}
		//,tags ={"@sprint117"}
		,tags ={"@sprint117", "@ingtest1"}
        ,monochrome=true
        		//@RunWith(Cucumber.class)
       // @Cucumber.Options(
             // format = {"html:target/cucumber-html-report", "json:target/cucumber-json-report.json"}
       // )
)
public class RunCIPSprint6Feature {

}

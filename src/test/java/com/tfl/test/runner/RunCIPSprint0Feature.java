package com.tfl.test.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * CHANGE : report name to something else or it may overwrite existing ones with same name
 */
@RunWith(Cucumber.class)
@CucumberOptions(format = { "pretty", "html:target/cip-sprint0-html-report","json:target/report_cip_sprint0.json" },
        glue = { "com.tfl.test.steps" },
        features = { "classpath:com/tfl/bdd/test/cip/"}
		,tags ={"@sprint0"}
		//,tags ={"@sprint0", "@wip"}
        ,monochrome=true
)
public class RunCIPSprint0Feature {
}

#WIP-215 JP updates page Summary
@sprint4
Feature: Update JP using Edit Travel Options and Accessibility

  Scenario Outline: edit jp for bus mode
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "bus" only mode and perform a search
    Then I should see the bus journey options

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  Scenario Outline: edit jp for tube
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "tube" only mode and perform a search
    Then I should see the tube journey option

    Examples: 
      | From                             | To                                |
      | Aldgate East Underground Station | Oxford Circus Underground Station |
      | Victoria                         | Bond Street Underground Station   |

  Scenario Outline: edit jp for dlr
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "dlr" only mode and perform a search
    Then I should see the dlr journey option

    Examples: 
      | From                 | To           |
      | Canary Wharf Station | Custom House |
      | east india           | Bank         |

  Scenario Outline: edit jp for BCH
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "cycle-hire" only mode and perform a search
    Then I should see the bch journey option

    Examples: 
      | From                           | To                                |
      | Phillimore Gardens, Kensington | Charlbert Street, St. John's Wood |
      | Broadcasting House, Marylebone | River Street , Clerkenwell        |

  Scenario Outline: edit jp for cycle
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "cycle" only mode and perform a search
    Then I should see the cycle journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  Scenario Outline: edit jp for river-bus
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "river-bus" only mode and perform a search
    Then I should see the river-bus journey option

    Examples: 
      | From           | To                   |
      | Bankside Pier  | Canary Wharf Pier    |
      | Greenwich Pier | St. Katharine's Pier |

  @wip
  Scenario Outline: edit jp for tram
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "tram" only mode and perform a search
    Then I should see the tram journey option

    Examples: 
      | From                          | To                            |
      | BLACKHORSE LANE TRAMLINK STOP | LEBANON ROAD TRAM STOP        |
      | SANDILANDS TRAM STOP          | HARRINGTON ROAD TRAMLINK STOP |

  Scenario Outline: edit jp for cable-car
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "cable-car" only mode and perform a search
    Then I should see the cable car journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  Scenario Outline: edit jp for overground
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "overground" only mode and perform a search
    Then I should see the overground journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  Scenario Outline: edit jp for national-rail
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "national-rail" only mode and perform a search
    Then I should see the national rail journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  Scenario Outline: edit jp for coach
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "coach" only mode and perform a search
    Then I should see the coach journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  Scenario Outline: edit jp for walking
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "walking" only mode and perform a search
    Then I should see the walking journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  Scenario Outline: edit jp for both bus and tube
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "bus" and "tube" mode and perform a search
    Then I should see the fatest journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  Scenario Outline: edit jp for tube,river-bus,overground and cycle-hire
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "tube" and "river-bus" mode and perform a search
    And I select "overground" and "cycle-hire" mode and perform a search
    Then I should see the fatest journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  Scenario Outline: edit jp for tube,river-bus,overground and walking
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "tube" and "river-bus" mode and perform a search
    And I select "overground" and "walking" mode and perform a search
    Then I should see the fatest journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

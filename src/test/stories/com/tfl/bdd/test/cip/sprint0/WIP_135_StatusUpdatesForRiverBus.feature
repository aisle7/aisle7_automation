#WIP-135 Riverbus status updates page
@sprint0
Feature: WIP-135 Status updates test for Riverbus

  Scenario: Check rivermap is displayed for River Bus
    Given User is in "River Bus" status updates page
    Then User should see "River Bus" map

  Scenario: Check status messages are displayed for River Bus
    Given User is in "River Bus" status updates page
    When User expands a random disruption for "River Bus"
    Then User see correct status message

  Scenario: Check disruption messages are displayed for River Bus
    Given User is in "River Bus" status updates page
    When User expands a random disruption for "River Bus"
    Then User see correct disruption message

  Scenario: Check status updates displayed for River Buss
    Given User is in "River Bus" status updates page
    When User expands a line "RB6" for mode "River Bus"
    Then User see correct status message for "RB6"

  Scenario Outline: Check status updates displayed for River Bus
    Given User is in "River Bus" status updates page
    When User expands a line "<river_bus>" for mode "River Bus"
    Then User see correct status message for "<river_bus>"

    Examples: 
      | river_bus           |
      | RB1                 |
      | RB2                 |
      | RB4                 |
      | RB5                 |
      | RB6                 |
      | Woolwich Free Ferry |

  Scenario: Check user is able to view weekend disruptions
    Given User is in "River Bus" status updates page
    When User selects "This weekend" from calendar
    Then User should see "River Bus" map

  Scenario Outline: Check user is able to view disruptions by selecting calendar dates
    Given User is in "<mode>" status updates page
    When User selects "<date>" from calendar
    Then User should see "<mode>" map

    Examples: 
      | mode      | date         |
      | River Bus | Later today  |
      | River Bus | This weekend |

#WIP-84
@sprint2
Feature: Show the line in route order where possible

  Scenario: verifying tube,DLR and Overground switch directions
    Given user in ssp page
    When user select specific tube for directions
    Then user able to see view stations by and switch directions

  Scenario: river bus switch directions
    Given user in ssp page
    When user select river bus option for journey and directions
    Then user able to see view stations by and switch directions for river bus

  Scenario: tram link switch directions
    Given user in ssp page
    When user select tram link option for journey and directions
    Then user able to see view stations by and switch directions for tram link

  Scenario: Emirates Air Line switch directions
    Given user in ssp page
    When user select Emirates Air Line option for journey and directions
    Then user able to see view stations by and switch directions for Emirates Air Line

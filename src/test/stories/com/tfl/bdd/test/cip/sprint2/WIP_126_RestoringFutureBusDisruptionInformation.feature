#WIP-126
@sprint2
Feature: Restoring future bus disruption information

  Scenario Outline: showing future bus disruption information in status update page
    Given user in Status Updates page
    When user select "<mode>" option from modes-dropdown-placeholder
    And user select bus number to find a bus stop or route
    And user select "<date>" form current date
    Then user able to see disruption bus stops

    Examples: 
      | mode | date         |
      | Bus  | This weekend |

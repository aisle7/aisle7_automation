#WIP-207 JP updates page
@sprint3
Feature: Plan A journey using a docking stations

   
  Scenario Outline: Select docking name From and To option in JP
    Given I am on Plan A Journey page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey button
    Then I should see the journey planner results page
    And I should see View Details

    Examples: 
      | From                           | To                            |
      | Phillimore Gardens, Kensington | New Globe Walk, Bankside      |
      | River Street , Clerkenwell     | Sedding Street, Sloane Square |

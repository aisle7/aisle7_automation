#WIP-538 JP updates page Summary
@sprint4
Feature: Plan cycle Journey to Find Appropriate route
 
   
  Scenario Outline: plan for cycle journey
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "cycle" only mode and perform a search
    Then I should see the cycle journey options list

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |
#WIP-8 Tramlink status updates page
@sprint0
Feature: WIP-8 Status updates test for Tramlink

  Scenario: Check trammap is displayed for Tramlink
    Given User is in "Tram" status updates page
    Then User should see "Tram" map

  Scenario: Check status messages are displayed for Tramlink
    Given User is in "Tram" status updates page
    When User expands a random disruption for "Tram"
    Then User see correct status message

  Scenario: Check disruption messages are displayed for Tramlink
    Given User is in "Tram" status updates page
    When User expands a random disruption for "Tram"
    Then User see correct disruption message

  Scenario: Check status updates displayed for Tramlinks
    Given User is in "Tram" status updates page
    When User expands a line "Tramlink 4" for mode "Tram"
    Then User see correct status message for "Tramlink 4"

  Scenario Outline: Check status updates displayed for Tramlinks
    Given User is in "Tram" status updates page
    When User expands a line "<tram_link>" for mode "Tram"
    Then User see correct status message for "<tram_link>"

    Examples: 
      | tram_link  |
      | Tramlink 1 |
      | Tramlink 2 |
      | Tramlink 3 |
      | Tramlink 4 |

  Scenario: Check user is able to view weekend disruptions
    Given User is in "Tram" status updates page
    When User selects "This weekend" from calendar
    Then User should see "Tram" map

  Scenario Outline: Check user is able to view disruptions by selecting calendar dates
    Given User is in "<mode>" status updates page
    When User selects "<date>" from calendar
    Then User should see "<mode>" map

    Examples: 
      | mode | date         |
      | Tram | Later today  |
      | Tram | This weekend |

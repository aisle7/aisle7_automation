#WIP-215 JP updates page Summary
@sprint6
Feature: Update JP using Edit Travel Options and Accessibility
@debugtest
  Scenario Outline: edit jp for bus mode
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "bus" only mode and perform a search
    Then I should see the bus journey options

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  @test
  Scenario Outline: edit jp for tube
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "tube" only mode and perform a search
    Then I should see the tube journey option

    Examples: 
      | From                             | To                                |
      | Aldgate East Underground Station | Oxford Circus Underground Station |
      | Victoria                         | Bond Street Underground Station   |

  Scenario Outline: edit jp for dlr
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "dlr" only mode and perform a search
    Then I should see the dlr journey option

    Examples: 
      | From                 | To           |
      | Canary Wharf Station | Custom House |
      | east india           | Bank         |

  @test543
  Scenario Outline: edit jp for river-bus
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "river-bus" only mode and perform a search
    Then I should see the river-bus journey option

    Examples: 
      | From           | To                   |
      | Bankside Pier  | Canary Wharf Pier    |
      | Greenwich Pier | St. Katharine's Pier |
@test121
  Scenario Outline: edit jp for tram
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "tram" only mode and perform a search
    Then I should see the tram journey option

    Examples: 
      | From                          | To                            |
      | BLACKHORSE LANE TRAMLINK STOP | LEBANON ROAD TRAM STOP        |
      | SANDILANDS TRAM STOP          | HARRINGTON ROAD TRAMLINK STOP |

  @test
  Scenario Outline: edit jp for cable-car
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "cable-car" only mode and perform a search
    Then I should see the cable car journey option

    Examples: 
      | From                         | To                   |
      | Emirates Greenwich Peninsula | Emirates Royal Docks |

  Scenario Outline: edit jp for overground
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "overground" only mode and perform a search
    Then I should see the overground journey option

    Examples: 
      | From                     | To                       |
      | Barking Rail Station     | Brondesbury Rail Station |
      | Brondesbury Rail Station | Forest Hill Rail Station |

  @test
  Scenario Outline: edit jp for national-rail
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "national-rail" only mode and perform a search
    Then I should see the national rail journey option

    Examples: 
      | From                 | To                                 |
      | Reading Rail Station | Victoria                           |
      | East Croydon         | Milton Keynes Central Rail Station |

  Scenario Outline: edit jp for coach
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "coach" only mode and perform a search
    Then I should see the coach journey option

    Examples: 
      | From     | To                              |
      | Victoria | Bond Street Underground Station |

  @krishna
  Scenario Outline: edit jp for walking
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select walking only option and perform a search
    Then I should see the walking journey option

    Examples: 
      | From                        | To                                | 
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  @krishna1
  Scenario Outline: edit jp for walking
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select fast walking only option and perform a search
    Then I should see the walking journey option

    Examples: 
      | From                        | To                                | 
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  @test123
  Scenario Outline: edit jp for walking
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select slow walking only option and perform a search
    Then I should see the walking journey option

    Examples: 
      | From                        | To                                | 
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |
@krishna3
  Scenario Outline: edit jp for both bus and tube
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "bus" and "tube" modes
    And I click on perform a search
    Then I should see the fatest journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  @test5
  Scenario Outline: edit jp for tube,river-bus,overground and cycle-hire
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "tube" and "river-bus" modes
    And I select "overground" and "tram" more modes
    And I click on perform a search
    Then I should see the fatest journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  @test5
  Scenario Outline: edit jp for tube,river-bus,overground and walking
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "tube" and "river-bus" modes
    And I select "overground" and "dlr" more modes
    And I click on perform a search
    Then I should see the fatest journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  @test123
  Scenario Outline: edit jp for BCH
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select cycle-hire only mode and perform a search
    Then I should see the bch journey option

    Examples: 
      | From                           | To                                |
      | Phillimore Gardens, Kensington | Charlbert Street, St. John's Wood |
      | Broadcasting House, Marylebone | River Street , Clerkenwell        |

  @test123
  Scenario Outline: edit jp for cycle
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select cycle only mode and perform a search
    Then I should see the cycle journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  @test123
  Scenario Outline: edit jp for cycle
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select take my cycle on PT mode and perform a search
    Then I should see the cycle journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  @test123
  Scenario Outline: edit jp for cycle
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select cycle LeaveAtStation mode and perform a search
    Then I should see the cycle journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  @test5
  Scenario Outline: edit jp for tube,river-bus,overground and walking
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "tube","river-bus","overground" and "tram" modes and perform a search
    Then I should see the fatest journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |

  @test1234
  Scenario Outline: edit jp for cycle
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select show me routes with fewest changes and perform a search
    Then I should see routes with fewest changes journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

  @test1234
  Scenario Outline: edit jp for cycle
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select Showing routes with least walking and perform a search
    Then I should see routes with least walking journey option

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

#WIP-399 ssp updates page
@sprint3
Feature: BCH docking station search

  Scenario Outline: Search docking station using nearby, maps-BCH, Find a docking station tool
    Given I am in SSP page
    When I select "<BusStop>" using auto suggest
    Then I able to see BCH transport from that station

    Examples: 
      | BusStop           |
      | Westminster Abbey |
      | Gray's Inn Road   |

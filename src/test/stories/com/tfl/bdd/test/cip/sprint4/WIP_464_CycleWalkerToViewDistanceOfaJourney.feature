#WIP-464 JP updates page Summary
@sprint4
Feature: Cycle and walker distance

    
  Scenario Outline: cycle jounrey distance
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "cycle" only mode and perform a search
    Then I should see the total cycle journey distance

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

   
  Scenario Outline: walking journey distance
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "walking" only mode and perform a search
    Then I should see the total walking journey distance

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

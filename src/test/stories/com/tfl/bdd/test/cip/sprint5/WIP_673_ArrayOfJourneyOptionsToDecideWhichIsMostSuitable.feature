#WIP-215 JP updates page
@sprint4
Feature: Array of journey options
         1.Fastest journey option
         2.Fewest changes
         3.Bus only journey options
         4.BCH journey option
         5.Personal Cycling journey option
         6.Walking journey option
         7.Step-free to the platform
  
  Scenario Outline: Array of journey options list
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    Then I should see the fatest journey option

    Examples: 
      | From                                       | To                                                             |
      | Reading Rail Station                       | Oxford Circus Underground Station                              |
      | london                                     | Bond Street Underground Station                                |
      | Current Location                           | Bond Street Underground Station                                |
      | Big Ben                                    | Southwark                                                      |
      | London City Airport                        | Harrow-on-the-Hill                                             |
      | heathrow terminal 5                        | Holborn,Holborn                                                |
      | Le May Avenue                              | tw134ge                                                       |
      | 55 broadway,westminster                    | w67lz                                                          |
      | trafalgar square                           | Buckingham Palace                                              |
      | TW15 1LW                                   | KT2 6NJ                                                        |
      | Emirates Greenwich Peninsula               | Emirates Royal Docks                                           |
      | Liverpool Street, Liverpool Street Station | Wembley Park Underground Station                               |
      | Green Park Underground Station             | Warren Street Underground Station                              |
      | Lewisham, Lewisham Station                 | Greenwich, University of Greenwich (Maritime Greenwich Campus) |
  
  Scenario Outline: Array of journey options list for BCH
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    Then I should see the array of JP options without walking only

    Examples: 
      | From                           | To                             |
      | Phillimore Gardens, Kensington | Broadcasting House, Marylebone |
      | River Street , Clerkenwell     | Sedding Street, Sloane Square  |
  
  Scenario Outline: BCH journey option only if I start and end my journey at a docking station.
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    Then I should see the dockstation journey result

    Examples: 
      | From                           | To                            |
      | Phillimore Gardens, Kensington | New Globe Walk, Bankside      |
      | River Street , Clerkenwell     | Sedding Street, Sloane Square |

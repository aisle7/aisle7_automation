#WIP-215 JP updates page
@sprint6
Feature: list of 3 Fast journey result along with following journey options
  1. Cycle 
  2. Walking
  3. Bus Only
  4. Cycle hire
  5. Least Walking
  6. Fewest Changes
  7. Full step free access route

  @krishna22
  Scenario Outline: Validating journey options list
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    Then I should see the Fastest jp result with other options

    Examples: 
      | From                           | To                                |
      | london                         | Bond Street Underground Station   |
      | Big Ben                        | Southwark                         |
      | trafalgar square               | Bond Street Underground Station   |
      | Green Park Underground Station | Warren Street Underground Station |

  @krishna22
  Scenario Outline: Array of journey options list for BCH
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    Then I should see the array of JP options without walking only

    Examples: 
      | From                         | To                   |
      | 55 broadway,westminster      | w67lz                |
      | TW15 1LW                     | KT2 6NJ              |
      | Emirates Greenwich Peninsula | Emirates Royal Docks |
      | heathrow terminal            | Holborn              |
      | Lewisham                     | Greenwich            |
      | Liverpool Street             | Wembley Park         |
      | London City Airport          | Harrow-on-the-Hill   |
      | Le May Avenue / Luffman Road | tw13 4ge              |

  @krishna22
  Scenario Outline: Array of journey options list
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    Then I should see the narional rail journey option

    Examples: 
      | From                 | To                                |
      | Reading Rail Station | Oxford Circus Underground Station |
      | East Croydon         | Weston Milton Rail Station        |

  @krishna22
  Scenario Outline: BCH journey option only if I start and end my journey at a docking station.
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    Then I should see the dockstation journey result

    Examples: 
      | From                           | To                            |
      | Phillimore Gardens, Kensington | New Globe Walk, Bankside      |
       
  Scenario Outline: Validating Bus only options list
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    And I click on bus only link from result page
    Then I should see the bus only result with other options

    Examples: 
      | From   | To                              |
      | london | Bond Street Underground Station |

  @wipf1
  Scenario Outline: Validating Cycle hire options list
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    And I click on Cycle hire link from result page
    Then I should see the Cycle hire result with other options

    Examples: 
      | From   | To                              |
      | london | Bond Street Underground Station |

  @wipf1
  Scenario Outline: Validating Least Walking option
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    And I click on Least Walking link from result page
    Then I should see the Least Walking result with other options

    Examples: 
      | From   | To                              |
      | london | Bond Street Underground Station |

  @wipf1
  Scenario Outline: Validating Fewest Changes option
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    And I click on Fewest Changes link from result page
    Then I should see the Fewest Changes result with other options

    Examples: 
      | From   | To                              |
      | london | Bond Street Underground Station |

  Scenario Outline: Validating Full step free access route options
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    And I click on Full step free access route link from result page
    Then I should see the Full step free access route result with other options

    Examples: 
      | From   | To                              |
      | london | Bond Street Underground Station |

  Scenario Outline: Validating Cycling option
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    And I click on Full Cycling link from result page
    Then I should see the Cycling result with other options

    Examples: 
      | From   | To                              |
      | london | Bond Street Underground Station |

  Scenario Outline: Validating walking option
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    And I click on Full Walking link from result page
    Then I should see the Walking result with other options

    Examples: 
      | From   | To                              |
      | london | Bond Street Underground Station |

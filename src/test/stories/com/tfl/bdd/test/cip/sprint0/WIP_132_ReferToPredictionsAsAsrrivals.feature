@sprint0
Feature: WIP-132 Check live departures is called live arrivals

  Scenario: Check home page text has changed to live arrivals
    Given I am in tfl home page
    Then I should see live arrivals text


  Scenario: Check SSP page search box text has changed to live arrivals
    Given I am in SSP page
    When I search for station "Paddington"
    Then I should see live arrivals text in search box

  Scenario Outline: Check SSP page search box text has changed to live arrivals
    Given I am in SSP page
    When I search for station "<station_name>"
    Then I should see live arrivals text in search box

    Examples: 
      | station_name                   |
      | Holborn |
      | Paddington |
      | Victoria |

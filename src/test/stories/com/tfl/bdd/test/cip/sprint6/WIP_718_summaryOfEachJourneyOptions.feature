#WIP-215 JP updates page Summary
@sprint7
Feature: Summary of Each Journey Options
  
   
  Scenario Outline: Array of journey options list summary
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    Then I should see the option type and edit button
    And mode of transport per journey leg involved
    And route for mode of transport
    And journey time in minutes and hours
    And disruption notification for journey leg if applicable

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station | 
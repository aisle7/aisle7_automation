@sprint0
Feature: WIP-50 Check user is able to zoom in and out on River Tours and River Bus map

  Scenario: Check user is able to navigate to River Tours map
    Given User is on river tours map page
    Then Check page title contains "River Tours"

  Scenario: Check user is able to navigate to River Bus map
    Given User is on river bus map page
    Then Check page title contains "River Bus"

  Scenario: Check user is able to zoom in on River Tours map
    Given User is on river tours map page
    When They click zoom in on river tours map
    Then Check map zoom in level changed

  Scenario: Check user is able to zoom out on River Tours map
    Given User is on river tours map page
    When They click zoom in on river tours map
    And They click zoom out on river tours map
    Then Check map zoom out level changed

  Scenario: Check user is able to zoom in on River Bus map
    Given User is on river bus map page
    When They click zoom in on river bus map
    Then Check map zoom in level changed

  Scenario: Check user is able to zoom out on River Bus map
    Given User is on river bus map page
    When They click zoom in on river bus map
    When They click zoom out on river bus map
    Then Check map zoom out level changed
    
    
    
#Perform full zoom in and zoom out : assumptions full zoom in (4) and full zoom out (0.4 and less)
  Scenario: Check user is able to perform full zoom in on River Tours map
    Given User is on river tours map page
    When They perform full zoom in on river tours map
    Then Check map zoom level is "4" or more
    
  Scenario: Check user is able to perform full zoom in and than full zoom out on River Tours map
    Given User is on river tours map page
    When They perform full zoom in on river tours map
    When They perform full zoom out on river tours map
    Then Check map zoom level is less than "0.4"
    
  Scenario: Check user is able to perform full zoom in on River Bus map
    Given User is on river bus map page
    When They perform full zoom in on river bus map
    Then Check map zoom level is "4" or more

  Scenario: Check user is able to perform full zoom in and than full zoom out on River Bus map
    Given User is on river bus map page
    When They perform full zoom in on river bus map
    When They perform full zoom out on river bus map
    Then Check map zoom level is less than "0.4"
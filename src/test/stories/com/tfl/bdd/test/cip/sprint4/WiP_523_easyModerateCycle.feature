#WIP-523 JP updates page Summary
@sprint4
Feature: easy,moderate and cycle verification

   
  Scenario Outline: journey for easy,moderate and cycle options
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "cycle" only mode and perform a search
    Then I should see the cycle journey options list

    Examples: 
      | From                             | To                                |
      | Aldgate East Underground Station | Oxford Circus Underground Station |
      | Victoria                         | Bond Street Underground Station   |

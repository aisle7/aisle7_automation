#WIP-215 JP updates page Summary
@sprint10
Feature: Update JP using Edit Travel Options and Accessibility

  Scenario Outline: edit jp for bus mode
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "bus" only mode and perform a search
    Then I should see the bus journey options

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

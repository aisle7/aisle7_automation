@sprint0
Feature: WIP-137 Lat/Long inversed in Nearby tool
@wip
  Scenario: Assuming user is in  St. James Park, Than correct current location should be displayed
    Given I am in st james park and on nearby page
    Then I should see bike icon with text "Abbey Orchard Street, Westminster" in nearby details map
    
 @wip   
  Scenario Outline: Check for Bike Docking Stations around london (Zoom In)
    Given I am on nearby page
    When I select "<location>" from auto suggest
   	Then I should see bike icon with text "<bikepoint>" in nearby details map
    
    Examples:
    | location |  zoom | image1 | bikepoint |
    | Heron Quay DLR Station | 0 | nearby_icon_bike_west_indie_quay_dlr_station.png | Heron Quay |
    | Piccadilly Circus Underground | 0 | nearby_icon_bike_piccadilly_circus_station.png | Panton Street |
    
    #| Embankment Underground Station | 0 | nearby_icon_bike_embankment_ug_station.png | Whitehall Place |
#WIP-206
@sprint2
Feature: plan a journey using Cycle option

  Scenario: showing easy, moderate and fast cycle journeys
    Given I am on Plan A Journey page
    When I select only cycle mode and perform a search
    And I enter From "Feltham" and To "Hounslow" stations
    Then I able to see Easy cycle journey options
    And I able to see Moderate cycle journey options
    And I able to see Fast cycle journey options

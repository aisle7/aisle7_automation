#WIP-398 Maps updates page
@sprint3
Feature: BCH docking station search

  Scenario Outline: Search docking station using nearby, maps-BCH, Find a docking station tool
    Given I am on nearby page
    When I select "<DockingStation>" from auto suggest
    Then I should see bike icon with text "<DockingStation>" in nearby details map

    Examples: 
      | DockingStation    |
      | St. Chad's Street |
      | King's Cross      |

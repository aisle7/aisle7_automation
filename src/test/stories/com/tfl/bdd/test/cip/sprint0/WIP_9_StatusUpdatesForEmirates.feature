#WIP-9 Emirates Air Line status updates map
@sprint0
Feature: WIP-9 Status updates test for Emirates

  Scenario: Check trammap is displayed for Emirates Air Line
    Given User is in "Emirates Air Line" status updates page
    Then User should see "Emirates Air Line" map

  Scenario: Check status messages are displayed for Emirates Air Line
    Given User is in "Emirates Air Line" status updates page
    When User expands a random disruption for "Emirates Air Line"
    Then User see correct status message

  Scenario: Check disruption messages are displayed for Emirates Air Line
    Given User is in "Emirates Air Line" status updates page
    When User expands a random disruption for "Emirates Air Line"
    Then User see correct disruption message

  Scenario: Check status updates displayed for Emirates Air Lines
    Given User is in "Emirates Air Line" status updates page
    When User expands a line "Emirates Air Line" for mode "Emirates Air Line"
    Then User see correct status message for "Emirates Air Line"

  Scenario: Check user is able to view weekend disruptions
    Given User is in "Emirates Air Line" status updates page
    When User selects "This weekend" from calendar
    Then User should see "Emirates Air Line" map

  Scenario Outline: Check user is able to view disruptions by selecting calendar dates
    Given User is in "<mode>" status updates page
    When User selects "<date>" from calendar
    Then User should see "<mode>" map

    Examples: 
      | mode              | date         |
      | Emirates Air Line | Later today  |
      | Emirates Air Line | This weekend |

#WIP-215 JP updates page Summary
@sprint4
Feature: Public Transport Journey to Find Appropriate route

   
  Scenario Outline: public transport journey validation
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I Unselect "bus" only mode and perform a search
    Then I should see the journey options

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |
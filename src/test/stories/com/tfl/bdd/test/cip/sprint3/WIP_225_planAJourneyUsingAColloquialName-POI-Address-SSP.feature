#WIP-225 JP updates page
@sprint3
Feature: Plan A journey using a colloquial name,POI, Address and SSP

  Scenario Outline: Select colloquial name From and To option in JP
    Given I am on Plan A Journey page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey button
    Then I should see the journey planner results page

    Examples: 
      | From                                                             | To                                                   |
      | The Gherkin;30 St Mary Axe                                       | King's Cross; Kings Cross, London N1                 |
      | The V&A; Victoria and Albert Museum Cromwell Road London SW7 2RL | NW1 5AL; Marylebone, London NW1 5AL                  |
      | Victoria                                                         | Victoria, London SW1V,UK i.e. Victoria train station |
      | TW13 4GE                                                         | EC1R 4PF                                             |
      | 55 Broadway                                                      | Grays In                                             |
      | London eye                                                       | the dome                                             |

#WIP-124
@sprint1
Feature: All headings and prose which references "Bus Route Maps" in http://tfl.gov.uk/maps_/bus-route-maps should be changed to "Bus Spider Maps"

  Scenario: The wording on the maps page for buses has changed
    Given user on the maps page
    When user click on Bus option from Maps page
    Then The wording on the button should be View Spider maps

  Scenario: The wording on the bus spider maps page for buses has changed
    Given user on the bus spider maps page
    Then The heading on the page should be Bus Spider maps

  Scenario: The wording on the bus spider maps page for buses has changed
    Given user on the bus spider maps page
    Then The description on the page should be Bus Spider maps

  Scenario: The wording on the bus spider maps page for buses has changed
    Given user on the bus spider maps page
    Then The search button titile on the page changed to Find Spider maps

  Scenario: The wording on the bus spider maps page for buses has changed
    Given user on the bus spider maps page
    Then The home view titile on the page changed to Bus Spider maps

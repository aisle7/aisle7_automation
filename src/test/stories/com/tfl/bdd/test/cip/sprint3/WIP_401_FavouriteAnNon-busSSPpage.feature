#WIP-401 favourites update in home screen
@sprint3
Feature: Favourites are adding to tube, trams, river and emirates air line

  Scenario Outline: Favourites are adding to tubes from ssp page
    Given I am in SSP page
    When I select Tube "<TubeStation>" using search option
    And I select as favourite of that station
    Then I able to see favourited station under tube option in home page

    Examples: 
      | TubeStation                      |
      | Baker Street Underground Station |
      | Vauxhall                         |

  Scenario Outline: Favourites are adding to buses from ssp page
    Given I am in SSP page
    When I select Bus "<BusStation>" using search option
    And I select as favourite of that station
    Then I able to see favourited station under bus option in home page

    Examples: 
      | BusStation        |
      | Westminster Abbey |

  Scenario Outline: Favourites are adding to river from ssp page
    Given I am in SSP page
    When I select River "<RiverStation>" using search option
    And I select as favourite of that station
    Then I able to see favourited station under river option in home page

    Examples: 
      | RiverStation      |
      | Westminster Abbey |

  Scenario Outline: Favourites are adding to tram from ssp page
    Given I am in SSP page
    When I select Tram "<TramStation>" using search option
    And I select as favourite of that station
    Then I able to see favourited station under tram option in home page

    Examples: 
      | TramStation       |
      | Westminster Abbey |

  Scenario Outline: Favourites are adding to Emirates Air Line from ssp page
    Given I am in SSP page
    When I select EAL "<EALStation>" using search option
    And I select as favourite of that station
    Then I able to see favourited station under EAL option in home page

    Examples: 
      | EALStation        |
      | Westminster Abbey |
      | Gray's Inn Road   |

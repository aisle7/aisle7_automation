@sprint0
Feature: WIP-239 Bus Disruptions disruption message to relate to all buses affected

  Scenario: I should see status alerts display multiple bus routes
    Given I am on Plan A Journey page
    When I enter From "West Croydon" and To "Lewisham" station
    And I select "bus" only mode and perform a search
    Then I should see journeys with multiple disruptions

  Scenario: I should see status alerts display multiple bus routes for weekend journeys
    Given I am on Plan A Journey page
    When I enter From "West Croydon" and To "Lewisham" station
    And Select weekend time
    And I select "bus" only mode and perform a search
    Then I should see journeys with multiple disruptions

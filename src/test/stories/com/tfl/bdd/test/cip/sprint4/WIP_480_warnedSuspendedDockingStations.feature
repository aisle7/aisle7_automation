#WIP-480 JP updates page Summary
@sprint4
Feature: Suspended Docking Stations

   
  Scenario Outline: JP for suspended docking stations
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "cycle-hire" only mode and perform a search
    And I click on suspended docking stations view details
    Then I should see suspended and relocated docking stations list

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |

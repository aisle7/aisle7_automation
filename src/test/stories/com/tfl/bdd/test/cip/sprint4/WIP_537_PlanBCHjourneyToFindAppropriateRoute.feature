#WIP-537 JP updates page Summary
@sprint4
Feature: Plan BCH journey to Find Appropriate Route

    
  Scenario Outline: Plan for BCH journey
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" station
    And I select "cycle-hire" only mode and perform a search
    Then I should see the cycle journey options list

    Examples: 
      | From                        | To                                |
      | Aldgate Underground Station | Oxford Circus Underground Station |
      | Victoria                    | Bond Street Underground Station   |
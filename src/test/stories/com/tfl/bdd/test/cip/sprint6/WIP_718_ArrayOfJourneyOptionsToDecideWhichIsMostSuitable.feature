#WIP-215 JP updates page

@sprint7

Feature: Array of journey options

  @wipf
  Scenario Outline: Array of journey options list
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    Then I should see the fatest journey option

    Examples: 
      | From                           | To                                |
      | london                         | Bond Street Underground Station   |
      | Big Ben                        | Southwark                         |
      | trafalgar square               | Buckingham Palace                 |
      | Green Park Underground Station | Warren Street Underground Station |

  @wip
  Scenario Outline: Array of journey options list
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    Then I should see the narional rail journey option

    Examples: 
      | From                 | To                                |
      | Reading Rail Station | Oxford Circus Underground Station |
      | East Croydon         | Weston Milton Rail Station        |
@wip21
  Scenario Outline: Array of journey options list for BCH
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    Then I should see the array of JP options without walking only

    Examples: 
      | From                           | To                             | 
      | 55 broadway,westminster        | w67lz                          |
      | TW15 1LW                       | KT2 6NJ                        |
      | Emirates Greenwich Peninsula   | Emirates Royal Docks           |
      | heathrow terminal 5            | Holborn                        |
      | Lewisham                       | Greenwich                      |
      | Liverpool Street               | Wembley Park                   |
      | London City Airport            | Harrow-on-the-Hill             |
      | Le May Avenue / Luffman Road   | tw134ge                        |

  @wip
  Scenario Outline: BCH journey option only if I start and end my journey at a docking station.
    Given I am on Plan A Journey new page
    When I enter From "<From>" and To "<To>" and click on Plan A Journey
    Then I should see the dockstation journey result

    Examples: 
      | From                           | To                            |
      | Phillimore Gardens, Kensington | New Globe Walk, Bankside      |
      | River Street , Clerkenwell     | Sedding Street, Sloane Square |

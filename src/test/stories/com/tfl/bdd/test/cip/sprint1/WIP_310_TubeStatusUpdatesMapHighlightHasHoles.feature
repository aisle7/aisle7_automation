#WIP-310
@sprint1
Feature: Tube status updates page should not have holes when disruptions are highlighted

  @wip
  Scenario Outline: User can able to see all the disruptions are highlighted with out any holes in Tube status updates page
    Given User at the Status Updates Page
    When User selects "<date>" from calendar
    Then Users should see "<tube>" map

    Examples: 
      | tube     | date         |
      | Bakerloo | Later today  |
      | Central  | This weekend |

#WIP-124
@sprint1
Feature: Showing stop closures on all bus route maps

  @wip
  Scenario: User should see which bus stop are disrupted using selection of bus number from stations-stops-and-piers page
    Given Assuming user select Buses option from stations-stops-and-piers page
    When user click on bus route number from the list of which bus are you looking for?
    Then user can able to see which bus stop are disrupted status in the bus stop list

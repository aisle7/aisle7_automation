package com.tfl.test.services;

import static com.jayway.restassured.RestAssured.get;

import java.util.List;

import com.jayway.restassured.response.Response;
import com.tfl.test.domain.Line;
import com.tfl.test.utils.common.JSONHelper;
import com.tfl.test.utils.common.ConfigUtils;


/**
 * Deals with all the necessary requests from the schematic map tests
 * @author Noor
 *
 */
public class SchematicMapsServiceImpl {

    private static final String REST_LINE_API = "Line";
	public static String targetAPIEndPoint =  null;
	
	public SchematicMapsServiceImpl() {
		if(targetAPIEndPoint == null){
			targetAPIEndPoint = ConfigUtils.getAPIEndPoint();
		}
	}

    /**
     * Query for lines from API
     * @param query
     * @return
     */
	public List<Line> getListOfLinesFromAPI(String query) {
		//make a call to api
		String apiURL = targetAPIEndPoint + REST_LINE_API + query ;
        System.out.println("\n" + apiURL);
		Response response = get(apiURL);
		String json = response.asString();
		System.out.println(json + "\n");
        List<Line> lines = JSONHelper.getLineDTOList(json);
		
		return lines;
	}
}

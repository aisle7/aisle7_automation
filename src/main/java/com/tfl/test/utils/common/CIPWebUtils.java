package com.tfl.test.utils.common;


import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.WebElement;

import com.tfl.test.domain.Disruption;
import com.tfl.test.domain.Line;
import com.tfl.test.services.SchematicMapsServiceImpl;


public class CIPWebUtils {



	private static SchematicMapsServiceImpl schematicMapService;


	/**
	 * Get zoom level
	 * @return
	 */
	public static double getZoomLevel(WebElement schematicMap) {
		String style = schematicMap.getAttribute("style");
		String zLevel = style.substring(style.indexOf("(")+1, style.indexOf(","));
		double zoomLevel = Double.parseDouble(zLevel);
		return zoomLevel;
	}
	

	public static String getStartEndDateTodayParameters() {
		String today = getToday();
		return "" + today + "/to/" + today;
	}
	
	public static String getChristmasDay(){
		Calendar when = Calendar.getInstance();
		int month = (when.get (Calendar.MONTH) + 2);
		int day = (when.get (Calendar.DAY_OF_MONTH) - 1);
		int year =when.get (Calendar.YEAR);
		//String thischristmas = year + "-" + month + "-" + day;
		//if (separteWithForwardSlash){
		String thischristmas = day + "/" + month + "/" + year;
		return thischristmas;
	}

	public static Calendar getNumberOfDaysTillWeekend() {
		Calendar ins = Calendar.getInstance();
		// 1=Sunday
		int day_of_week = ins.get(Calendar.DAY_OF_WEEK);

		// 1=Sunday, 7=Saturday
		if (day_of_week == 1 || day_of_week == 7) {
			// already in weekend : if sunday than go back a day
			if (day_of_week == 1)
				ins.add(Calendar.DAY_OF_WEEK, -1);
		} else {
			int number_of_days_till_weekend = 7 - day_of_week;
			ins.add(Calendar.DAY_OF_WEEK, number_of_days_till_weekend);
		}
		return ins;
	}

	
	public static String getToday() {
		Calendar daysTillWeekend = Calendar.getInstance();
		int date = daysTillWeekend.get(Calendar.DATE);
		int month = (daysTillWeekend.get(Calendar.MONTH) + 1);
		int year = daysTillWeekend.get(Calendar.YEAR);
		String saturday = year + "-" + month + "-" + date;
		return saturday;
	}

	
	public static String getLaterToday() {
		Calendar daysTillWeekend = Calendar.getInstance();
		int date = daysTillWeekend.get(Calendar.DATE);
		int month = (daysTillWeekend.get(Calendar.MONTH) + 1);
		int year = daysTillWeekend.get(Calendar.YEAR);
		String laterToday = add0IfNeeded(date) + "/" + add0IfNeeded(month) + "/" + year;
		return laterToday;
	}

	public static String getDateOnSaturday(boolean separteWithForwardSlash) {
		Calendar daysTillWeekend = getNumberOfDaysTillWeekend();
		int date = daysTillWeekend.get(Calendar.DATE);
		int month = (daysTillWeekend.get(Calendar.MONTH) + 1);
		int year = daysTillWeekend.get(Calendar.YEAR);
		String saturday = year + "-" + month + "-" + date;
		if(separteWithForwardSlash){
			saturday = add0IfNeeded(date) + "/" + add0IfNeeded(month) + "/" + year;
		}
		return saturday;
	} 
	
	public static String getDateOnSunday(boolean separteWithForwardSlash) {
		Calendar daysTillWeekend = getNumberOfDaysTillWeekend();
		daysTillWeekend.add(Calendar.DATE, 1);
		int date = daysTillWeekend.get(Calendar.DATE);
		int month = (daysTillWeekend.get(Calendar.MONTH) + 1);
		int year = daysTillWeekend.get(Calendar.YEAR);
		String saturday = year + "-" + month + "-" + date;
		if(separteWithForwardSlash){
			saturday = add0IfNeeded(date) + "/" + add0IfNeeded(month) + "/" + year;
		}
		return saturday;
	}

	private static String add0IfNeeded(int dd) {
		if(dd <= 9){
			return "0" + dd;
		}
		return "" + dd;
	}


	public static String getFutureDateOnSaturday(int numberOfWeeks) {
		Calendar daysTillWeekend = getNumberOfDaysTillWeekend();
		daysTillWeekend.add(Calendar.DAY_OF_YEAR, 7 * numberOfWeeks);

		int date = daysTillWeekend.get(Calendar.DATE);
		int month = (daysTillWeekend.get(Calendar.MONTH) + 1);
		int year = daysTillWeekend.get(Calendar.YEAR);
		String saturday = year + "-" + month + "-" + date;
		return saturday;
	}

	public static String getFutureDateOnSunday(int numberOfWeeks) {
		Calendar daysTillWeekend = getNumberOfDaysTillWeekend();
		daysTillWeekend.add(Calendar.DATE, 1);
		daysTillWeekend.add(Calendar.DAY_OF_YEAR, 7 * numberOfWeeks);
		int date = daysTillWeekend.get(Calendar.DATE);
		int month = (daysTillWeekend.get(Calendar.MONTH) + 1);
		int year = daysTillWeekend.get(Calendar.YEAR);
		String saturday = year + "-" + month + "-" + date;
		return saturday;
	}


	public static String getModeId(String mode) {
		String id = "tram";
		if(mode.equals("Tram")){
			id = "tram";
		}else if(mode.equals("River Bus")){
			id = "river-bus";
		}else if(mode.equals("Emirates Air Line")){
			id = "cable-car";
		}else if(mode.equals("Tube")){
			id = "tube";
		}else if(mode.equals("Bus")){
			id = "bus";
		}else if(mode.equals("Roads")){
			id = "streets";
		} 
		
		return id;
	}


	public static List<Disruption> getRandomDisruption(String mode) {
		if(schematicMapService == null)
		schematicMapService = new SchematicMapsServiceImpl();
		//String query = "/tube,dlr,overground/Line/PlannedWorks/" + StatusUtils.getStartEndDateParameters();
		//String query = "?startDate= &endDate= &modes=tube,dlr,overground&lineIds= ";
		String query = "/Mode/" + mode + "/Status/" + CIPWebUtils.getStartEndDateTodayParameters();
		List<Line> listOfLinesFromAPI = schematicMapService.getListOfLinesFromAPI(query);
		List<Disruption> disruptions = getRandomDisruptionFromList(listOfLinesFromAPI);
		return disruptions;
	}

	

	public static Line getRandomLineWithDisruption(String mode) {
		if(schematicMapService == null)
		schematicMapService = new SchematicMapsServiceImpl();
		//String query = "/tube,dlr,overground/Line/PlannedWorks/" + StatusUtils.getStartEndDateParameters();
		//String query = "?startDate= &endDate= &modes=tube,dlr,overground&lineIds= ";
		String query = "/Mode/" + mode + "/Status/" + CIPWebUtils.getStartEndDateTodayParameters();
		List<Line> listOfLinesFromAPI = schematicMapService.getListOfLinesFromAPI(query);
		Line line = getRandomLineWithDisruptionFromList(listOfLinesFromAPI);
		return line;
	}
	


	public static List<Line> getListOfLineWithDisruption(String mode) {
		if(schematicMapService == null)
		schematicMapService = new SchematicMapsServiceImpl();
		String query = "/Mode/" + mode + "/Status/" + CIPWebUtils.getStartEndDateTodayParameters();
		List<Line> listOfLinesFromAPI = schematicMapService.getListOfLinesFromAPI(query);
		return listOfLinesFromAPI;
	}

	public static List<Disruption> getRandomDisruptionFromList(List<Line> listOfLinesFromAPI) {
		if(listOfLinesFromAPI!=null && listOfLinesFromAPI.size() > 0){
			int size = listOfLinesFromAPI.size();
			int random = (int )(Math.random() * size );
			Line line = listOfLinesFromAPI.get(random);
			List<Disruption> disruptionList = line.getDisruptionList();
			return disruptionList;
		}
		return null;
	}
	
	public static Line getRandomLineWithDisruptionFromList(List<Line> listOfLinesFromAPI) {
		if(listOfLinesFromAPI!=null && listOfLinesFromAPI.size() > 0){
			int size = listOfLinesFromAPI.size();
			int random = (int )(Math.random() * size );
			Line line = listOfLinesFromAPI.get(random);
			return line;
		}
		return null;
	}


	public static Disruption getRandomDisruptionFromLine(List<Disruption> disruptionList) {
		if(disruptionList!=null && disruptionList.size() > 0){
			int size = disruptionList.size();
			int random = (int )(Math.random() * size );
			Disruption disruption = disruptionList.get(random);
			return disruption;
		}
		return null;
	}


	public static String getWeekendDateId() {
		String saturday = getDateOnSaturday(true);
		String sunday = getDateOnSunday(true);
		return saturday + "-" + sunday;
	}
}

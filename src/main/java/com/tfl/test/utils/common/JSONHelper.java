package com.tfl.test.utils.common;

import static com.jayway.restassured.path.json.JsonPath.from;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.jayway.restassured.path.json.JsonPath;
import com.tfl.test.domain.Disruption;
import com.tfl.test.domain.Line;


/**
 * 
 */
public class JSONHelper {

    private static JsonArray array = new JsonArray();
    private static Gson gson = new Gson();
    private static JsonParser parser = new JsonParser();


	public static List<Line> getLineDTOList(String rootResponse) {
		List<Line> lineDTO = new ArrayList<Line>();
		List<Disruption> disruptionsList = new ArrayList<Disruption>();
		String lineId = "";
		String lineName = "";
		String lineResponse = "";
		String lineStatus = "";
		String lineStatusesForThisLine = "";
		String modeName = "";
		if (!"[null]".equals(rootResponse)) {
			List<Map<String, String>> lineRoot = from(rootResponse).get("");
			for (int i = 0; i < lineRoot.size(); i++) {
				lineResponse = String.valueOf(lineRoot.get(i));
				lineName = String.valueOf(lineRoot.get(i).get("name"));
				lineId = String.valueOf(lineRoot.get(i).get("id"));
				final List<String> lineStatusForRoot = from(lineResponse)
						.getList("lineStatuses");
				
				//get list of reasons
				List<String> reasons = getListOfReasons(lineStatusForRoot);
				lineStatusesForThisLine = from(lineResponse)
						.get("lineStatuses").toString();
				if (!StringUtils.contains("[null]",
						from(lineStatusesForThisLine).get("disruption")
								.toString())) {
					disruptionsList = constructDisruptionDTO(lineStatusesForThisLine);
				}
				

				modeName = String.valueOf(lineRoot.get(i).get("modeName"));
				constructLineStatusDTO(lineDTO, disruptionsList, lineName,
						lineStatus, lineStatusForRoot, lineId, modeName);
				
				updateSetOfReasons(lineDTO, lineName, reasons);
			}
		}
		return lineDTO;
	}

	private static void updateSetOfReasons(List<Line> lineDTO, String lineName,
			List<String> reasons) {
		Line line = null;
		//find line
		for(Line l : lineDTO){
			if(l.getName().equals(lineName)){
				line = l;
				line.setReasons(reasons);
				//System.out.println("Updated Line With Reasons ");
				break;
			}
		}
	}


	private static List<Disruption> constructDisruptionDTO(
			String lineStatusesForThisLine) {
		List<Disruption> disruptionsList = new ArrayList<Disruption>();
		String disruptionForThisLine = "";
		String affectedStopPointsByDisruption = "";
		disruptionForThisLine = from(lineStatusesForThisLine).get(
				"disruption.description").toString();
		affectedStopPointsByDisruption = from(lineStatusesForThisLine).get(
				"disruption.affectedStops.name").toString();
		Disruption disruption = new Disruption();
		disruption.setReason(disruptionForThisLine.substring(1,
				disruptionForThisLine.length() - 1));
		disruption.setStopPoints(affectedStopPointsByDisruption.substring(1,
				affectedStopPointsByDisruption.length() - 1));
		disruptionsList.add(disruption);
		return disruptionsList;
	}

	private static List<String> getListOfReasons(List<String> lineStatusForRoot) {
		List<String> reasons = new ArrayList<String>();
		 for (Object ap : lineStatusForRoot) {
	            JsonPath jp2 = new JsonPath(ap.toString());

	            String key = jp2.getString("reason");
	            if(key != null){
		            reasons.add(key);
	            }
	        }
		return reasons;
	}

	private static void constructLineStatusDTO(List<Line> lineDTO,
			List<Disruption> disruptionsList, String lineName,
			String lineStatus, final List<String> lineStatusForRoot, String lineId, String modeName) {
		String json = "";
		String testReason = "";
		HashSet<String> ts = new HashSet<String>();
		for (int j = 0; j < lineStatusForRoot.size(); j++) {
			json = "[" + String.valueOf(lineStatusForRoot.get(j)) + "]";
			array = parser.parse(json).getAsJsonArray();
			Line event = gson.fromJson(array.get(0), Line.class);
			ts.add(event.getStatusSeverityDescription());
			if (lineStatusForRoot.size() > 1) {
				lineStatus = lineStatus
						+ ts.toString()
								.trim()
								.substring(1, ts.toString().trim().length() - 1);
				if (null != event.getReason() && event.getReason().length() > 0) {
					testReason = testReason
							+ event.getReason()
									.substring(
											event.getReason().indexOf(":") + 1,
											event.getReason().length()).trim();
				}
			} else {
				if (null != event.getReason() && event.getReason().length() > 0) {
					testReason = event
							.getReason()
							.substring(event.getReason().indexOf(":") + 1,
									event.getReason().length()).trim();
				}
			}
			if (j + 1 == lineStatusForRoot.size()) {
				Line line = new Line();
				lineStatus = ts.toString().trim()
						.substring(1, ts.toString().trim().length() - 1);
				if (StringUtils.endsWithIgnoreCase("059", lineName)
						|| StringUtils.endsWithIgnoreCase("London Overground",
								lineName)) {
					//lineName = "Overground";
				}
				line.setName(lineName);
				line.setStatusSeverityDescription(lineStatus);
				line.setDistruptionsCount(lineStatusForRoot.size());
				line.setReason(testReason);
				line.setDisruptionList(disruptionsList);
				line.setModeName(modeName);
				lineStatus = "";
				testReason = "";
				line.setId(lineId);
				lineDTO.add(line);
			}
		}
	}
}

package com.tfl.test.utils.common;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;


/**
 * If you are using slowloadablecomponent way of creating and managing 
 * page objects than make use of this
 * @author Noor
 *
 */
public class IsLoaded {
    private final WebDriver driver;

    public IsLoaded(WebDriver driver) {
        this.driver = driver;
    }

    public static IsLoaded forThis(WebDriver driver) {
        IsLoaded isLoaded = new IsLoaded(driver);
        return isLoaded;
    }


    public IsLoaded whenElementIsDisplayed(WebElement usingBy, String description) {
        try{
            if(usingBy.isDisplayed()){
                return this;
            }else{
                throw new Error(description);
            }
        }catch(WebDriverException e){
            throw new Error(description + " is not displayed", e);
        }
    }

    public IsLoaded whenElementIsEnabled(WebElement usingBy, String description) {
        try{
            if(usingBy.isEnabled()){
                return this;
            }else{
                throw new Error(description);
            }
        }catch(WebDriverException e){
            throw new Error(description + " is not enabled", e);
        }
    }


    public IsLoaded whenElementIsSelected(WebElement usingBy, String description) {
        try{
            if(usingBy.isSelected()){
                return this;
            }else{
                throw new Error(description);
            }
        }catch(WebDriverException e){
            throw new Error(description + " is not enabled", e);
        }
    }
    
    public IsLoaded whenElementIsDisplayed(By usingBy, String description) {
        try{
            if(driver.findElement(usingBy).isDisplayed()){
                return this;
            }else{
                throw new Error(description);
            }
        }catch(WebDriverException e){
            throw new Error(description + " is not displayed", e);
        }
    }

    public IsLoaded whenElementIsEnabled(By usingBy, String description) {
        try{
            if(driver.findElement(usingBy).isEnabled()){
                return this;
            }else{
                throw new Error(description);
            }
        }catch(WebDriverException e){
            throw new Error(description + " is not enabled", e);
        }
    }


    public IsLoaded whenElementIsSelected(By usingBy, String description) {
        try{
            if(driver.findElement(usingBy).isSelected()){
                return this;
            }else{
                throw new Error(description);
            }
        }catch(WebDriverException e){
            throw new Error(description + " is not enabled", e);
        }
    }
}

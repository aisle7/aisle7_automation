package com.tfl.test.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.slf4j.Logger;

import com.tfl.test.utils.common.ConfigUtils;


/**
 * This will automatically wait for part of the page to load
 * @author Noor
 *
 */
public class TFLHomePage{


	private static Logger log = null;
	static{
		if(log == null){
			log = ConfigUtils.getLogger(TFLHomePage.class);
		}
	}
	
	@FindBy(how = How.CSS, using = "input.primary-button.plan-journey-button")
	private WebElement planAJourneyButton;
	
	@FindBy(how = How.ID, using="From")
	private WebElement fromStation;
	
	@FindBy(how = How.ID, using="To")
	private WebElement toStation;
	
	@FindBy(how=How.CSS, using="a.flat-button.top-icon.external-link")
	WebElement liveArrivals;

	private WebDriver driver;

	public TFLHomePage(WebDriver driver) {
        ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 30);
        PageFactory.initElements(finder, this);
        this.driver = driver;
	}
	
	public PlanAJourneyPage clickPlanAJourneyButtonAndFail(){
		planAJourneyButton.click();
		return PageFactory.initElements(driver, PlanAJourneyPage.class);//new PlanAJourneyPage(driver);
	}
	
	public JourneyPlannerResultsPage clickPlanAJourneyButtonAndPass(){
		planAJourneyButton.click();
		return PageFactory.initElements(driver, JourneyPlannerResultsPage.class); //new JourneyPlannerResultsPage(driver);
	}
	
	public void clickPlanAJourney(){
		planAJourneyButton.click();
	}
	
	public void enterFromStation(String name){
		fromStation.sendKeys(name);
	}
	
	public void enterToStation(String name){
		toStation.sendKeys(name);
	}

	public boolean checkFromErrorMessageIsDisplayed() {
		List<WebElement> errors = driver.findElements(By.className("field-validation-error"));
		WebElement fromErrorMessage = errors.get(1);
		return fromErrorMessage.isDisplayed();
	}

	public boolean checkToErrorMessageIsDisplayed() {
		List<WebElement> errors = driver.findElements(By.className("field-validation-error"));
		WebElement fromErrorMessage = errors.get(2);
		return fromErrorMessage.isDisplayed();
	}

	public boolean checkWeAreInHomePage() {
		String title = driver.getTitle();
		log.info(title);
		return title.equals("Transport for London");
	}

	public boolean checkLiveArrivalsLinksIsDisplayed() {
		String text = liveArrivals.getText().replace("<br/>", "").replace("\n", "");
		return text.equals("Livearrivals");
	}

}

package com.tfl.test.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;

import com.tfl.test.utils.common.ConfigUtils;

/**
 * You only need to use these methods
 */
public class PlanAJourneyPage{


	@FindBy(how=How.CLASS_NAME, using="change-departure-time")
	WebElement changeTime;
	
	@FindBy(how = How.CSS, using = "input.primary-button.plan-journey-button")
	private WebElement planAJourneyButton;
	
	@FindBy(how = How.CSS, using = ".primary-button.plan-journey-button")
	WebElement planMyJourney;
	
	//@FindBy(how = How.ID, using="From")
	@FindBy(how = How.ID, using="InputFrom")
	private WebElement fromStation;
	
	//@FindBy(how = How.ID, using="To")
	@FindBy(how = How.ID, using="InputTo")
	private WebElement toStation;
	
	@FindBy(how = How.ID, using="Date")
	private WebElement dateDropDown;
	
	@FindBy(how=How.CLASS_NAME, using="stop-name")
	WebElement stop;
	
	@FindBy(how=How.CLASS_NAME, using="stop-name")
	List<WebElement> stops;
	
	@FindBy(how=How.CLASS_NAME, using="mode-icon bch-docking-station-icon")
	WebElement bchIcon;

	@FindBy(how=How.CSS, using="a.toggle-options.more-options")
	WebElement moreOption;
	
	@FindBy(how=How.CSS, using="div .modes-of-transport li input")
	List<WebElement> listOfModes;
	
	@FindBy(how=How.CSS, using=".suspended-docking-station-list.start-hidden.journey-details>div>h2")
	List<WebElement> suspendedDockingStationList;
	
	@FindBy(how=How.CSS, using=".primary-button.suspended-docking-station-list-button.show-detailed-results")
	WebElement suspendedDockingStationButton;
	
	@FindBy(how=How.CSS, using="div .route-detail")
	List<WebElement> cycleRouteDetails;
	
	@FindBy(how=How.CSS, using=".clearfix.total-distance")
	List<WebElement> totalDistance;
	
	@FindBy(how=How.CSS, using=".walking-distance")
	List<WebElement> totalWalkingDistance;
	
	@FindBy(how=How.CSS, using="span.jp-multiple-journey-type")
	List<WebElement> listOfJPModes;
	
	@FindBy(how=How.CSS, using=".secondary-button.edit-button")
	WebElement editbutton; 
	
	@FindBy(how=How.CSS, using=".tube-icon.hide-text")
	List<WebElement> tubeicon;
	
	@FindBy(how=How.CSS, using=".title")
	List<WebElement> departsArrives;
	
	@FindBy(how=How.CSS, using=".journey-time")
	List<WebElement> journeyTimeMins;
	
	@FindBy(how=How.CSS, using=".walking-icon.hide-text")
	List<WebElement> walkingIcon;
	
	@FindBy(how=How.CSS, using=".cycle-icon.hide-text")
	List<WebElement> cycleIcon;
	
	@FindBy(how=How.CSS, using=".bus-icon.hide-text")
	List<WebElement> busIcon;
	
	@FindBy(how=How.CSS, using=".time")
	List<WebElement> timeIcon;
	
	@FindBy(how=How.CSS, using=".disruption.hide-text")
	List<WebElement> disruptionsIcon;
	
	@FindBy(how=How.CSS, using="div .jp-multiple-journey-type")
	WebElement listOfJPMode;
	
	@FindBy(how=How.CSS, using=".icon-left.cycle")
	WebElement cycleMode; 
	
	@FindBy(how=How.CLASS_NAME, using="cycle-text")
	WebElement easyCycle;
	
	@FindBy(how=How.CLASS_NAME, using="cycle-text")
	WebElement moderateCycle;
	
	@FindBy(how=How.CLASS_NAME, using="cycle-text")
	WebElement fastCycle;
	
	@FindBy(how=How.CLASS_NAME, using="jp-multiple-journey-type")
	WebElement bchJourneyType;
	
	@FindBy(how=How.CLASS_NAME, using="jp-multiple-journey-type")
	List<WebElement> bchJourneyTypes;
	
	@FindBy(how = How.XPATH, using=".//*[@id='full-width-content']/div/div[4]/div/div[1]/div[1]/div/div[1]/div[3]/button")
	WebElement easyViewDetails;
	
	@FindBy(how = How.XPATH, using=".//*[@id='full-width-content']/div/div[4]/div/div[1]/div[2]/div/div[1]/div[3]/button")
	WebElement moderateViewDetails;
	
	@FindBy(how = How.XPATH, using=".//*[@id='full-width-content']/div/div[4]/div/div[1]/div[3]/div/div[1]/div[3]/button")
	WebElement fastViewDetails;
	
	@FindBy(how=How.CLASS_NAME, using="jp-bch-availability")
	WebElement bchAvailability;
	
	@FindBy(how=How.XPATH, using=".//*[@id='full-width-content']/div/div[5]/div/div/div/div/div[1]/div/a/div/div[1]/span")
	WebElement fasttestByPublicTransport;
	
	///////New JP Changes/////
	
	@FindBy(how=How.CSS, using=".expandable-box.click-through.auto-expand.publictransport-box")
	List<WebElement> listOfjourneySteps;
	
	@FindBy(how=How.CSS, using=".expandable-box.click-through.auto-expand.walking-box")
	List<WebElement> listOfWalkingSteps;
	
	@FindBy(how=How.CSS, using=".clearfix.time-boxes")
	List<WebElement> listOfTimeBoxes;
	
	@FindBy(how=How.CSS, using=".price-and-details.clearfix")
	List<WebElement> listOfViewDetails;
	
	@FindBy(how=How.CSS, using=".plain-button.with-icon")
	List<WebElement> listOfallJourneys;
	
	@FindBy(how=How.CSS, using=".journey-box")
	List<WebElement> cycleAndWalkingJP;
	
	@FindBy(how=How.CSS, using=".select-deselect-option.jp-deselect-all>a")
	WebElement deselectAllModes;
	
	@FindBy(how=How.CSS, using=".dlr-icon.hide-text")
	List<WebElement> dlrIcon;
	
	@FindBy(how=How.CSS, using=".river-bus-icon.hide-text")
	List<WebElement> riverBusIcon;
	
	@FindBy(how=How.CSS, using=".tram-icon.hide-text")
	List<WebElement> tramIcon;
	
	@FindBy(how=How.CSS, using=".travelpreferences-cycling")
	List<WebElement> travelPreferencesCycling;
	
	@FindBy(how=How.CSS, using=".walking")
	WebElement walkingOption;
	
	@FindBy(how=How.CSS, using="label.boxed-label-for-input[for='slow']")
	WebElement slowWalking;
	
	@FindBy(how=How.CSS, using="label.boxed-label-for-input[for='average']")
	WebElement averageWalking;
	
	@FindBy(how=How.CSS, using="label.boxed-label-for-input[for='fast']")
	WebElement fastWalking;
	
	@FindBy(how=How.CSS, using="label.boxed-label-for-input[for='AllTheWay']")
	WebElement cycleAllTheWay;
	
	@FindBy(how=How.CSS, using="label.boxed-label-for-input[for='CycleHire']")
	WebElement cycleHire;
	
	@FindBy(how=How.CSS, using="label.boxed-label-for-input[for='TakeOnTransport']")
	WebElement cycleTakeOnTransport;
	
	@FindBy(how=How.CSS, using="label.boxed-label-for-input[for='LeaveAtStation']")
	WebElement cycleLeaveAtStation;
	
	@FindBy(how=How.XPATH, using=".//*[@id='JourneyPreference']/option[2]")
	WebElement showmeleastinterchange;
	
	@FindBy(how=How.XPATH, using=".//*[@id='JourneyPreference']/option[3]")
	WebElement showmeleastwalking;
	
	@FindBy(how=How.CSS, using=".plain-button.with-icon[title='Bus Only']")
	WebElement busOnlyOption;
	
	@FindBy(how=How.CSS, using=".plain-button.with-icon[title='Cycle Hire']")
	WebElement cycleHireOption;
	
	@FindBy(how=How.CSS, using=".plain-button.with-icon[title='Least Walking']")
	WebElement leastWalkingOption;
	
	@FindBy(how=How.CSS, using=".plain-button.with-icon[title='Fewest Changes']")
	WebElement fewestChangesOption;
	
	@FindBy(how=How.CSS, using=".plain-button.with-icon[title='Full Step Free Access']")
	WebElement fullStepFreeAccessRouteOption;
	
	@FindBy(how=How.CSS, using=".journey-box[data-tracking-value='JP: Cycling']")
	WebElement cyclingOption;
	
	@FindBy(how=How.CSS, using=".journey-box[data-tracking-value='JP: WalkingOnly']")
	WebElement walkingOnlyOption;
	
	private WebDriver driver;
	private static Logger log = null;
	static{
		if(log == null){
			log = ConfigUtils.getLogger(PlanAJourneyPage.class);
		}
	}

	public PlanAJourneyPage(WebDriver driver) {
        ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 30);
        PageFactory.initElements(finder, this);
        this.driver = driver;
	}
	
	public PlanAJourneyPage clickPlanAJourneyButtonAndFail(){
		planAJourneyButton.click();
		return PageFactory.initElements(driver, PlanAJourneyPage.class);//new PlanAJourneyPage(driver);
	}
	
	public JourneyPlannerResultsPage clickPlanAJourneyButtonAndPass(){
		planAJourneyButton.click();
		return PageFactory.initElements(driver, JourneyPlannerResultsPage.class); //new JourneyPlannerResultsPage(driver);
	}
	
	public JourneyPlannerResultsPage clickPlanAJourney(){
		planAJourneyButton.click();
		return new JourneyPlannerResultsPage(driver);
	}
	
	public void clickPlanMyJourney(){
		planMyJourney.click();
	} 
	
	public void clickAverageWalkOption(){
		moreOption.click();
		walkingOption.click();
		averageWalking.click(); 
	}
	
	public void clickCycleHireOption(){
		moreOption.click();
		cycleMode.click();
		cycleHire.click();
	}
	
	public void clickCycleAllTheWay(){
		moreOption.click();
		cycleMode.click();
		cycleAllTheWay.click();
	}
	
	public void clickCycleTakeOnTransport(){
		moreOption.click();
		cycleMode.click();
		cycleTakeOnTransport.click();
	}
	
	public void clickCycleLeaveAtStation(){
		moreOption.click();
		cycleMode.click();
		cycleLeaveAtStation.click();
	}
	
	public void clickFastWalking(){
		moreOption.click();
		walkingOption.click();
		fastWalking.click(); 
	}
	public void clickSlowWalking(){
		moreOption.click();
		walkingOption.click();
		slowWalking.click(); 
	}
	
	public void clickShowMeleastinterchange(){
		moreOption.click();
		showmeleastinterchange.click(); 
	}
	public void clickShowMeleastwalking(){
		moreOption.click();
		showmeleastwalking.click(); 
	}
	
	public void enterFromStation(String name){
		fromStation.sendKeys(name);
	}
	
	public void enterToStation(String name){
		toStation.sendKeys(name);
	}

	public boolean checkWeAreInJourneyPage() {
		String title = driver.getTitle();
		log.info(title);
		return title.equals("Plan a journey - Transport for London");
	}

	public void selectFromDropDownOptions(String location) {
		for(WebElement el : stops){
			String text = el.getText();
			if(text.contains(location)){
				stop = el;
				break;
			}
		}
		Actions action = new Actions(driver);
		action.moveToElement(stop).click().build().perform();
		
	}
	public void selectFromDropDownOptionsBCH(String location) {
		for(WebElement el : stops){
			String text = el.getText();
			boolean id = bchIcon.isSelected();
			if(text.contains(location) && id){
				stop = el;
				break;
			}
		}
		Actions action = new Actions(driver);
		action.moveToElement(stop).click().build().perform();
		
	}
	
	public void selectMode(String mode) {
		moreOption.click();
		uncheckAllModes();
		selectSepecifiedMode(mode);
	}
	
	public PlanAJourneyPage unselectallModes(String mode){
		moreOption.click();
		deselectAllModes.click();
		//selectSepecifiedMode(mode);
		return new PlanAJourneyPage(driver);
	}
	
	public void unselectMode(String mode) {
		moreOption.click();
		//uncheckAllModes();
		selectSepecifiedMode(mode);
	}
	
	public void selectSepecifiedMode(String mode) {
		//WebElement bus = driver.findElement(By.cssSelector(".bus.boxed-label-for-input.odd"));
		WebElement bus = driver.findElement(By.cssSelector("."+mode));
		bus.click();
		//WebElement el = driver.findElement(By.id(mode)); 
		//el.click();
		//Actions ac = new Actions(driver);
		//ac.click(el).build().perform();
		//ac.moveToElement(el).clickAndHold().release().build().perform();
	}

	public void selectModes(String mode, String mode2) {
		moreOption.click();
		//uncheckAllModes();
		deselectAllModes.click();
		selectSepecifiedModes(mode, mode2);
	}
	
	public void selectforModes(String mode, String mode2, String mode3, String mode4) {
		moreOption.click();
		//uncheckAllModes();
		deselectAllModes.click();
		selectSepecifiedforModes(mode,mode2,mode3,mode4);
	}
	public void selectSepecifiedModes(String mode, String mode2) {
		WebElement el = driver.findElement(By.cssSelector("."+mode));
		//WebElement el = driver.findElement(By.id(mode));
		WebElement el1 = driver.findElement(By.cssSelector("."+mode2));
		//WebElement el1 = driver.findElement(By.id(mode2));
		//WebElement el = driver
		el.click();
		el1.click();
		//Actions ac = new Actions(driver);
		//ac.click(el).build().perform();
		//ac.click(el1).build().perform();
	}
	public void selectSepecifiedforModes(String mode, String mode2,String mode3, String mode4 ) {
		WebElement el = driver.findElement(By.cssSelector("."+mode));
		WebElement el1 = driver.findElement(By.cssSelector("."+mode2));
		WebElement el2 = driver.findElement(By.cssSelector("."+mode3));
		WebElement el3 = driver.findElement(By.cssSelector("."+mode4));
		//WebElement el = driver.findElement(By.id(mode));
		//WebElement el1 = driver.findElement(By.id(mode2));
		//WebElement el2 = driver.findElement(By.id(mode3));
		//WebElement el3 = driver.findElement(By.id(mode4));
		//WebElement el = driver
		Actions ac = new Actions(driver);
		ac.click(el).build().perform();
		ac.click(el1).build().perform();
		ac.click(el2).build().perform();
		ac.click(el3).build().perform();
	}
	
	private void uncheckAllModes() {
		for(WebElement el: listOfModes){
			log.info(el.isSelected() + ", " + el.getAttribute("type"));
			if(el.isSelected()){
				//el.click();
				Actions ac = new Actions(driver);
				ac.click(el).build().perform();
			}
		}
	}

	
	
	public void selectWeekendTime() {
		changeTime.click();
		selectAWeekendTime();
	}

	public void selectCycleMode(){
		cycleMode.click();
	}
	private void selectAWeekendTime() {
		dateDropDown.click();
		Select select = new Select(dateDropDown);
		List<WebElement> options = select.getOptions();
		WebElement weekendDate = null;
		for(WebElement el: options){
			String text = el.getText();
			if(text.contains("Sat") || text.contains("Sun")){
				weekendDate = el;
				break;
			}
		}
		
		//Now select it
		String date = weekendDate.getText();
		Select dropdown = new Select(dateDropDown);
		dropdown.selectByVisibleText(date);
	}
	public boolean checkEasyCycleText() {
		String text = easyCycle.getText();
		log.info("Text : " + text);
		boolean contains = text.contains("Easy");
		return contains;
	}
	public boolean checkModerateCycleText() {
		String text = moderateCycle.getText();
		log.info("Text : " + text);
		boolean contains = text.contains("Moderate");
		return contains;
	}
	public boolean checkFastCycleText() {
		String text = fastCycle.getText();
		log.info("Text : " + text);
		boolean contains = text.contains("Fast");
		return contains;
	}
	
	public boolean FastestByPublicTransport() { 
		String text = listOfJPMode.getText();
		log.info("Text : " + text);
		boolean contains = text.contains("Fastest by public transport");
		return contains;
	}

	public boolean editButtonText() {
		String text = editbutton.getText();
		log.info("Text : " + text);
		boolean contains = text.contains("Edit");
		return contains;
	}
	
	public boolean moreOptionsAccessibility(){
		String text = moreOption.getText();
		log.info("Text : " + text);
		boolean contains = text.contains("Travel options & accessibility");
		return contains; 
	}
	
	
	
public boolean JpTransportDisplayed() {
	
		boolean isMultipleDisplayed = false; 
		for(WebElement el: listOfJPModes){
			String status = el.getText();
			if (status.contains(status)) 
				
				System.out.println(status);
			
				isMultipleDisplayed = true; 
				break; 
		}
		return isMultipleDisplayed;
	} 

public List<String> TubeiconDisplayedTest() {
	List<String> allJPTransportList = new ArrayList<String>();
	for (WebElement e : tubeicon) { 
		allJPTransportList.add(e.getText());
			System.out.println(e.getText()); 
		}   
	return allJPTransportList;
}

public List<String> departsArrivesDisplayedTest() {
	List<String> allJPTransportList = new ArrayList<String>();
	for (WebElement e : departsArrives) { 
		allJPTransportList.add(e.getText());
			System.out.println(e.getText()); 
		}   
	return allJPTransportList;
}

public List<String> journeyTimeMinsTest() {
	List<String> allJPTransportList = new ArrayList<String>();
	for (WebElement e : journeyTimeMins) { 
		allJPTransportList.add(e.getText());
			System.out.println(e.getText()); 
		}   
	return allJPTransportList;
}

public List<String> walkingIconDisplayTest() {
	List<String> allJPTransportList = new ArrayList<String>();
	for (WebElement e : walkingIcon) { 
		allJPTransportList.add(e.getText());
			System.out.println(e.getText()); 
		}   
	return allJPTransportList;
}

public List<String> cycleIconDisplayTest() {
	List<String> allJPTransportList = new ArrayList<String>();
	for (WebElement e : cycleIcon) { 
		allJPTransportList.add(e.getText());
			System.out.println(e.getText()); 
		}   
	return allJPTransportList;
}

public List<String> dlrIconDisplayTest() {
	List<String> allJPTransportList = new ArrayList<String>();
	for (WebElement e : dlrIcon) { 
		allJPTransportList.add(e.getText());
			System.out.println(e.getText()); 
		}   
	return allJPTransportList;
}

public List<String> busIconDisplayTest() {
	List<String> allJPTransportList = new ArrayList<String>();
	for (WebElement e : busIcon) { 
		allJPTransportList.add(e.getText());
			System.out.println(e.getText()); 
		}   
	return allJPTransportList;
} 
public List<String> riverBusIconDisplayTest() {
	List<String> allJPTransportList = new ArrayList<String>();
	for (WebElement e : riverBusIcon) { 
		allJPTransportList.add(e.getText());
			System.out.println(e.getText()); 
		}   
	return allJPTransportList;
}

public List<String> tramIconDisplayTest() {
	List<String> allJPTransportList = new ArrayList<String>();
	for (WebElement e : tramIcon) { 
		allJPTransportList.add(e.getText());
			System.out.println(e.getText()); 
		}   
	return allJPTransportList;
}

public List<String> timeIconDisplayTest() {
	List<String> allJPTransportList = new ArrayList<String>();
	for (WebElement e : timeIcon) { 
		allJPTransportList.add(e.getText());
			System.out.println(e.getText()); 
		}   
	return allJPTransportList;
}

public List<String> disruptionsIconDisplayTest() {
	List<String> allJPTransportList = new ArrayList<String>();
	for (WebElement e : disruptionsIcon) { 
		allJPTransportList.add(e.getText());
			System.out.println(e.getText()); 
		}   
	return allJPTransportList;
} 
public List<String> cycleRouteDetailsTest() {
	List<String> allJPTransportList = new ArrayList<String>();
	for (WebElement e : cycleRouteDetails) { 
		allJPTransportList.add(e.getText());
			System.out.println(e.getText()); 
		}   
	return allJPTransportList;
}

public List<String> totalDistanceText() {
	List<String> allJPTransportList = new ArrayList<String>();
	for (WebElement e : totalDistance) { 
		allJPTransportList.add(e.getText());
			System.out.println(e.getText()); 
		}   
	return allJPTransportList;
}

public List<String> totalWalkingDistanceText() {
	List<String> allJPTransportList = new ArrayList<String>();
	for (WebElement e : totalWalkingDistance) { 
		allJPTransportList.add(e.getText());
			System.out.println(e.getText()); 
		}   
	return allJPTransportList;
} 

public List<String> suspendedDockstationList() {
	List<String> allJPTransportList = new ArrayList<String>();
	for (WebElement e : suspendedDockingStationList) { 
		allJPTransportList.add(e.getText());
			System.out.println(e.getText()); 
		}   
	return allJPTransportList;
}

public void suspendedDockstationButton(){
	suspendedDockingStationButton.click();	
}
public List<String> getTIMKeyValues() {
    List<String> keyValues=new ArrayList<String>();
    for(WebElement e:listOfJPModes){
    //
      if( e.isDisplayed()) {
          keyValues.add(e.getText().trim());
          System.out.println(keyValues);
      }
    }
    return keyValues;
}


    public void clickEasyViewDetails(){
		easyViewDetails.click();
	} 
	public void clickModerateViewDetails(){
		moderateViewDetails.click();
	}
	public void clickFastViewDetails(){
		fastViewDetails.click();
	}
	public boolean verifyBCHAvailability(){
		String text = bchAvailability.getText(); 
		log.info("Text : " + text);
		boolean contains = text.contains("bikes available"); 
		return contains; 
	}
	public boolean verifyBCHSpaces(){
		String text = bchAvailability.getText(); 
		log.info("Text : " + text);
		boolean contains = text.contains("spaces"); 
		return contains; 
	}
	public void clickOnBCHJourneyType(){ 
		for(WebElement el : bchJourneyTypes){
			String text = el.getText();
			if(text.contains("Barclays Cycle Hire")){
				bchJourneyType = el;
				break;
			}
		}
		Actions action = new Actions(driver);
		action.moveToElement(bchJourneyType).click().build().perform();
		
	}
	
	////for new JP Design Changes ///////
	public List<String> JpTransportDisplayedTest() {
		List<String> allJPTransportList = new ArrayList<String>();
		for (WebElement e : listOfjourneySteps) { 
			//allJPTransportList.add(e.getAttribute("style"));
			allJPTransportList.add(e.getText());
				//System.out.println(e.getCssValue("style")); 
			}   
		return allJPTransportList;
	}
	 
	public List<String> JpWalkingDisplayedTest() {
		List<String> allJPTransportList = new ArrayList<String>();
		for (WebElement e : listOfWalkingSteps) { 
			//allJPTransportList.add(e.getAttribute("style"));
			allJPTransportList.add(e.getText());
				//System.out.println(e.getCssValue("style")); 
			}   
		return allJPTransportList;
	}
	
	public List<String> JpListofTimeBoxesTest() {
		List<String> allJPTransportList = new ArrayList<String>();
		for (WebElement e : listOfTimeBoxes) { 
			allJPTransportList.add(e.getText());
				System.out.println(e.getText()); 
			}   
		return allJPTransportList;
	}

	public List<String> JpListofViewDetailsTest() {
		List<String> allJPTransportList = new ArrayList<String>();
		for (WebElement e : listOfViewDetails) { 
			allJPTransportList.add(e.getText());
				System.out.println(e.getText()); 
			}   
		return allJPTransportList;
	}

	public List<String> ListofAllOtherJpModesTest() {
		List<String> allJPTransportList = new ArrayList<String>();
		for (WebElement e : listOfallJourneys) { 
			allJPTransportList.add(e.getText());
				System.out.println(e.getText()); 
			}   
		return allJPTransportList;
	}
	
	public List<String> ListofCycleAndWalkingJPTest() {
		List<String> allJPTransportList = new ArrayList<String>();
		for (WebElement e : cycleAndWalkingJP) { 
			allJPTransportList.add(e.getText());
				System.out.println(e.getText()); 
			}   
		return allJPTransportList;
	}
	
	public void deselectAllModestest(){
		deselectAllModes.click();
	}
	
	public void busOnlyOptionLinkTest(){
		busOnlyOption.click();
	}
	
	public void cycleHireOptionLinkTest(){
		cycleHireOption.click();
	}
	public void leastWalkingOptionLinkTest(){
		leastWalkingOption.click();
	}
	public void fewestChangesOptionLinkTest(){
		fewestChangesOption.click();
	}
	public void fullSTepFreeAccessRouteOptionLinkTest(){
		fullStepFreeAccessRouteOption.click();
	}
	public void cyclingOptionLinkTest(){
		cyclingOption.click();
	}
	public void walkingOnlyOptionLinkTest(){
		walkingOnlyOption.click();
	}
	//Aisle7 pageObjects
	@FindBy(how = How.CSS, using=".hni_username-field")
	private WebElement logintohomescreen;
	
	@FindBy(how = How.CSS, using=".hni_password-field")
	private WebElement passwordtohomescreen;
	
	@FindBy(how = How.CSS, using=".hni_btn")
	private WebElement clickontologinbutton;
	
	@FindBy(how = How.CSS, using=".hni_logo.hni_text--norm")
	private WebElement formulationmanagerText;
	
	//@FindBy(how = How.CSS, using=".hni_btn")
	//private WebElement clickontologinbutton;
	
	public void loginToMainScreen(String login){
		logintohomescreen.sendKeys(login);
	}
	
	public void passwordToMainScreen(String login){
		passwordtohomescreen.sendKeys(login);
	}
	
	public void clickOnLoginButton(){
		clickontologinbutton.click();
	}
	public boolean homeScreenText() {
		String text = formulationmanagerText.getText();
		log.info("Text : " + text);
		boolean contains = text.contains("FORMULATION MANAGER");
		return contains;
	}
	
	@FindBy(how = How.CSS, using=".hni_nav-items a[href='#/Ingredients']")
	private WebElement ingredientresearch;
	
	@FindBy(how = How.CSS, using=".hni_nav-sub-nav-group")
	private WebElement ingredientresearch1;
	//.hni_nav-sub-nav-group
	 
	public void clickIngredientResearchLink(){
		ingredientresearch1.click();
		ingredientresearch.click();
	}
	
	
}

package com.tfl.test.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.slf4j.Logger;

import com.tfl.test.utils.common.ConfigUtils;


/**
 * You only need to use these methods
 */
public class JourneyPlannerResultsPage {

	private WebDriver driver;
	private static Logger log = null;
	static{
		if(log == null){
			log = ConfigUtils.getLogger(JourneyPlannerResultsPage.class);
		}
	}

	@FindBy(how=How.CSS, using=".start-hidden.summary .journey-steps .disrupted a")
	List<WebElement> statusAlerts;
	
	@FindBy(how=How.CSS, using=".start-hidden.summary .journey-steps .disrupted a")
	WebElement anAlerts;
	
	@FindBy(how=How.CLASS_NAME, using="primary-button.show-detailed-results")
	WebElement viewDetails;
	
	public JourneyPlannerResultsPage(WebDriver driver) {
        ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 30);
        PageFactory.initElements(finder, this);
        this.driver = driver;
	}
	
	/**
	 * Simple check to make sure we are in correct page
	 * @param expectedTitle
	 * @return
	 */
	public boolean checkTitleMatches(String expectedTitle){
		String title = driver.getTitle();
		return title.equals(expectedTitle);
	}
	
	public String getTitle(){
		return driver.getTitle();
	}

	public boolean checkTitleMatches() {
		String title = driver.getTitle();
		log.info(title);
		return title.equals("Journey results - Transport for London");
	}
	
	public boolean checkMultipleDisruptionsDisplayed() {
		
		//new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(anAlerts));
		
		boolean isMultipleDisplayed = false;
		for(WebElement el: statusAlerts){
			String status = el.getText();
			int numberOfRoutes = getRouteCount(status);
			if(numberOfRoutes > 1){
				log.info("Multiple routes found : " + status);
				isMultipleDisplayed = true;
				break;
			}
		}
		return isMultipleDisplayed;
	}

	private int getRouteCount(String status) {
		String[] data = status.replace(",", "").split(" ");
		int count = 0;
		for(String d: data){
			try{
				int num = Integer.parseInt(d);
				count++;
			}catch(Exception e){
			}
		}
		return count;
	}
}

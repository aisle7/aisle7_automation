package com.tfl.test.pages.map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.slf4j.Logger;

import com.tfl.test.utils.common.CIPWebUtils;
import com.tfl.test.utils.common.ConfigUtils;


/**
 * This will automatically wait for part of the page to load
 * @author Noor
 *
 */
public class MapSection{


	private static Logger log = null;
	static{
		if(log == null){
			log = ConfigUtils.getLogger(MapSection.class);
		}
	}
	
	private WebDriver driver;

	@FindBy(how=How.ID, using="schematic-map")
	WebElement schematicMap;

	//@FindBys({@FindBy(how=How.ID, using="schematic-map"),@FindBy(how=How.CSS, using=".field-validation-error")})
	//WebElement schematicMap;
	
	@FindBy(how=How.CLASS_NAME, using="zoom-in")
	WebElement zoomIn;
	
	@FindBy(how=How.CLASS_NAME, using="zoom-out")
	WebElement zoomOut;

	private double prevZoomLevel;

	public MapSection(WebDriver driver) {
        ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 30);
        PageFactory.initElements(finder, this);
        this.driver = driver;
	}
	
	public boolean checkWeAreInRiverToursPage() {
		String title = driver.getTitle();
		log.info(title);
		return title.equals("Transport for London");
	}

	/**
	 * Click zoom in and keep track of zoom level
	 */
	public void clickZoomIn() {
		prevZoomLevel = CIPWebUtils.getZoomLevel(schematicMap);
		zoomIn.click();
	}
	
	public void clickZoomOut() {
		prevZoomLevel = CIPWebUtils.getZoomLevel(schematicMap);
		zoomOut.click();
	}

	public boolean checkZoomLevelHaveIncreased() {
		double currentZoomLevel = CIPWebUtils.getZoomLevel(schematicMap);
		boolean increased = currentZoomLevel > prevZoomLevel;
		prevZoomLevel = currentZoomLevel;
		return increased;
	}


	public boolean checkZoomLevelHaveDecreased() {
		double currentZoomLevel = CIPWebUtils.getZoomLevel(schematicMap);
		boolean increased = currentZoomLevel < prevZoomLevel;
		prevZoomLevel = currentZoomLevel;
		return increased;
	}

	public void fullZoomIn() {
		boolean increased = true;
		do{
			zoomIn.click();
			increased = checkZoomLevelHaveIncreased();
		}while(increased);
		log.info("done");
	}

	public boolean checkCurrentZoomLevelIsAsExpected(String zoomLevel) {
		double expectedLevel = Double.parseDouble(zoomLevel);
		return expectedLevel == prevZoomLevel;
	}

	public void fullZoomOut() {
		boolean decreased = false;
		do{
			clickZoomOut();
			decreased = checkZoomLevelHaveDecreased();
		}while(decreased);
		log.info("done");
	}

	public boolean checkCurrentZoomOutLevelIsAround(String zoomLevel) {
		double expectedLevel = Double.parseDouble(zoomLevel);
		return prevZoomLevel <= expectedLevel;
	}
	
	
}

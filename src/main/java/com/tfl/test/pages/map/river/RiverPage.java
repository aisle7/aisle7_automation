package com.tfl.test.pages.map.river;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.slf4j.Logger;

import com.tfl.test.utils.common.ConfigUtils;


/**
 * This will automatically wait for part of the page to load
 * @author Noor
 *
 */
public class RiverPage{


	private static Logger log = null;
	static{
		if(log == null){
			log = ConfigUtils.getLogger(RiverPage.class);
		}
	}
	private WebDriver driver;

	public RiverPage(WebDriver driver) {
        ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 30);
        PageFactory.initElements(finder, this);
        this.driver = driver;
	}
	
	public boolean checkWeAreInRiverPage() {
		String title = driver.getTitle();
		log.info(title);
		return title.equals("Transport for London");
	}

}

package com.tfl.test.pages;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.slf4j.Logger;

import com.tfl.test.utils.common.ConfigUtils;

/**
 * Created by balajiravivarman on 09/10/2014.
 */
public class FaresAndPaymentPage {

    private WebDriver driver;
    private static Logger log = null;
	static{
		if(log == null){
			log = ConfigUtils.getLogger(FaresAndPaymentPage.class);
		}
	}

    @FindBy(how = How.ID , using = "From")
    private WebElement from;

    @FindBy(how = How.ID , using = "To")
    private WebElement to;

    @FindBy(how = How.ID , using = "PassengerType")
    private WebElement passengerType;

    @FindBy(how = How.CLASS_NAME , using = "primary-button")
    private WebElement checkFare;

    public FaresAndPaymentPage(WebDriver driver) {
        ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 30);
        PageFactory.initElements(finder, this);
        this.driver = driver;
    }

    public void findFare(String fromStation, String toStation) {
          from.sendKeys(fromStation);
          to.sendKeys(toStation);
          checkFare.submit();

    }

    public boolean checkFares() {
        boolean overAllCheckStatus = Boolean.FALSE;
        List<WebElement> faresTypes = driver.findElements(By.cssSelector(".label"));
        String fareTypes = "";
        if (faresTypes.size() > 0) {
            for (int i=0; i < faresTypes.size(); i++) {
                fareTypes = fareTypes + " " + faresTypes.get(i).getText();
            }
            if (StringUtils.containsIgnoreCase(fareTypes,"oyster") || StringUtils.containsIgnoreCase(fareTypes, "cash")) {
                overAllCheckStatus = Boolean.TRUE;
            }
        }
        return overAllCheckStatus;
    }

    public boolean checkWeAreAtSFFPage() {
        String title = driver.getTitle();
        log.info(title);
        return title.equals("Single fare finder - Transport for London");
    }
}

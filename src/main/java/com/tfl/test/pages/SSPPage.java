package com.tfl.test.pages;

import java.util.ArrayList;
import java.util.List;

import javax.xml.crypto.KeySelector;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.slf4j.Logger;

import com.tfl.test.utils.common.ConfigUtils;


/**
 * This will automatically wait for part of the page to load
 * @author Noor
 *
 */
public class SSPPage{

	private static Logger log = null;
	static{
		if(log == null){
			log = ConfigUtils.getLogger(SSPPage.class);
		}
	}
	
	private WebDriver driver;

	@FindBy(how=How.ID, using="Input")
	WebElement inputField;
	
	@FindBy(how=How.CLASS_NAME, using="stop-name")
	WebElement stop;
	
	@FindBy(how=How.CLASS_NAME, using="stop-name")
	List<WebElement> stops;
	
	@FindBy(how=How.CSS, using=".search-filter>p")
	WebElement searchBoxText;
	
	@FindBy(how=How.CSS, using=".disruption-icon")
	WebElement disruptionIcon;
	
	@FindBy(how=How.CSS, using=".primary-button")
	WebElement goButton;
	
	@FindBy(how=How.CSS, using=".bus-icon")
	WebElement busesrouteSelector; 
	
	@FindBy(how=How.XPATH, using=".//*[@id='routeSelector']/div[2]/div[2]/div/ul/li[1]/div/a")
	WebElement whichbusesroutelooking; 
	
	@FindBy(how=How.XPATH, using=".//*[@id='routeSelector']/div[2]/div[2]/div/ul/li[2]/div/a")
	WebElement busrouteselect2;
	
	@FindBy(how=How.XPATH, using=".//*[@id='routeSelector']/div[2]/div[2]/div/ul/li[6]/ul/li[44]/a")
	WebElement busroute95;
	
	@FindBy(how=How.XPATH, using=".//*[@id='routeSelector']/div[2]/div[2]/div/ul/li[5]/ul/li[41]/a")
	WebElement selectingbusRoute;
	
	@FindBy(css = ".disrupted")
	private List<WebElement> disrubtedStops;
	
	@FindBy(how=How.CSS, using=".accordion-heading.with-icon.primary")
	WebElement barclaysCycleHire;
	
	@FindBy(how=How.CSS, using=".link-button.with-icon.primary")
	WebElement transportSelect;
	
	public SSPPage(WebDriver driver) {
        ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 30);
        PageFactory.initElements(finder, this);
        this.driver = driver;
	}
	
	public boolean checkWeAreInHomePage() {
		String title = driver.getTitle();
		log.info(title);
		return title.equals("Stations, Stops and Piers");
	}

	public void searchForStation(String location) {
		log.info("Enter search term : "+ location);
		inputField.sendKeys(location);
	}
	 
	public SSPPage selectFromDropDownOptions(String location) {
		log.info("Select location from drop down : " + location);
		for(WebElement el : stops){
			String text = el.getText();
			if(text.contains(location)){ 
				stop = el; 
				//el.click();
				break; 
			}
		}  
		Actions action = new Actions(driver); 
		action.moveToElement(stop).click().build().perform();
		//action.sendKeys(stop).click().build().perform();
		//action.build();
		//action.perform();
		//return new SSPPage(driver); 
		return new SSPPage(driver);
	}
	public boolean onMouseOver(WebElement element)
	{
		boolean result = false;
		try
		{
			String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript(mouseOverScript, element);
			result = true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			result = false;
		}
		return result;
	}
	 
	
	public boolean checkLiveArrivalTextShownInSearchBox() {
		String text = searchBoxText.getText();
		log.info("Text : " + text);
		boolean contains = text.contains("live arrivals");
		return contains;
	}
	
	public void busRouteSelect(){
		busesrouteSelector.click(); 
	}
	
	public void whichbusrouteLooking(){
		whichbusesroutelooking.click(); 
	}
	
	public void selectingbusnumber(){
		selectingbusRoute.click();
	}

	public void selectbusroute2(){
		busrouteselect2.click();
	}
	
	public void selectbusnumber(){
		busroute95.click();
	}
	
	public void enterBusNumber(){
		inputField.sendKeys("95");
	}
	
	public void clickOnGoButton(){
		goButton.click();
	}
	public List<String> getAllDisrubtedStops() {
		List<String> allDisrubtedStopsList = new ArrayList<String>();
		for (WebElement e : disrubtedStops) {
			allDisrubtedStopsList.add(e.getText());
		}
		return allDisrubtedStopsList;
	}
	public boolean bchTextValidations(){ 
		String text = barclaysCycleHire.getText();
		log.info("Text : " + text);
		boolean contains = text.contains("Barclays Cycle Hire");
		return contains;
	}
	public void bchexpand(){
		barclaysCycleHire.click();
	}
	
	public void transportSelection(){
		transportSelect.getAttribute("Tube");
		
	}
}

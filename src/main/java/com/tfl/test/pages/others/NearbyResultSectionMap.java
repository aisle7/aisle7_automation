package com.tfl.test.pages.others;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;

import com.tfl.test.utils.common.ConfigUtils;


/**
 * Nearby : SearchBoxSection NearbyMapSection NearbyFilterSection
 * 
 * @author Noor
 * 
 */
public class NearbyResultSectionMap{

	WebDriver driver;
	private static Logger log = null;
	static{
		if(log == null){
			log = ConfigUtils.getLogger(NearbyResultSectionMap.class);
		}
	}
	
	@FindBy(how=How.CLASS_NAME, using="map-html-marker")
	List<WebElement> markers;

	public NearbyResultSectionMap(WebDriver driver) {
        ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 30);
        PageFactory.initElements(finder, this);
        this.driver = driver;
	}
	
	public boolean checkWeAreInNearbyPage() {
		String title = driver.getTitle();
		log.info(title);
		return title.equals("Nearby");
	}


	public boolean checkForBikeIconOnMapPanelInfo(String bikePointName) {
		log.info("Looking for bikepoint : " + bikePointName);
		
		new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By
				.className("map-html-marker")));
		
		List<WebElement> listOfRiverMarkers = new ArrayList<WebElement>();

		// Find only bikepoint markers
		for (WebElement el : markers) {
			String title = el.getAttribute("title");
			log.info(title);
			if (title.contains(bikePointName)) {
				listOfRiverMarkers.add(el);
				WebElement tag = el.findElement(By.tagName("div"));
				if (tag != null
						&& tag.getAttribute("id").contains("BikePoints_"))
					break;
			}
		}

		boolean exists = false;
		// Check pier exists
		for (WebElement el : listOfRiverMarkers) {
			WebElement tag = el.findElement(By.tagName("div"));
			log.info(tag.getAttribute("id"));
			if (tag != null && tag.getAttribute("id").contains("BikePoints_")) {
				exists = true;
				break;
			}
		}
		return exists;
	}
	

}

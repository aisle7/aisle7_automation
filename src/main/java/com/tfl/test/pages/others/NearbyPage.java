package com.tfl.test.pages.others;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.slf4j.Logger;

import com.tfl.test.utils.common.ConfigUtils;


/**
 * Nearby : 
 * 	SearchBoxSection
 *  NearbyMapSection
 *  NearbyFilterSection
 * 	
 * @author Noor
 *
 */
public class NearbyPage{

    WebDriver driver;
	private static Logger log = null;
	static{
		if(log == null){
			log = ConfigUtils.getLogger(NearbyPage.class);
		}
	}
	
	@FindBy(how=How.ID, using="Input")
	WebElement searchField;
	
	@FindBy(how=How.CLASS_NAME, using="remove-content")
	List<WebElement> removeIcons;
	
	@FindBy(how=How.CLASS_NAME, using="stop-name")
	WebElement stop;
	
	@FindBy(how=How.CLASS_NAME, using="stop-name")
	List<WebElement> stops;
	
	@FindBy(how=How.CSS, using=".mode-icon.bch-docking-station-icon")
	WebElement dockStationIcon;

    public NearbyPage(WebDriver driver){
        ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 30);
        PageFactory.initElements(finder, this);
        this.driver = driver;
	}
	
	public boolean checkWeAreInNearbyPage() {
		String title = driver.getTitle();
		log.info(title);
		return title.equals("Nearby");
	}

	public void clickOnSearchBox() {
		log.info("Click Search Box");
		searchField.click();
	}

	public void clearSearchField() {
		log.info("Clear Search Box");
		//click on remove content
		WebElement removeContent = null;
		if(removeIcons.size() > 1){
			removeContent = removeIcons.get(1);
		}else if(removeIcons.size() == 1){
			removeContent = removeIcons.get(0);
		}
		if(removeContent!=null && removeContent.isEnabled() && removeContent.isDisplayed())
		removeContent.click();
		
		if(searchField!=null && !searchField.getText().equals("")){
			removeContent.click();
		}
		
	}
	

	public void enterSearchTerm(String location) {
		log.info("Enter Search Term : "+ location);
		WebElement input = driver.findElement(By.id("Input"));
		input.sendKeys(location);
	}
	
	public void selectFromDropDownOptions(String location) {
		log.info("Select location from drop down options");
		for(WebElement el : stops){
			String text = el.getText();
			if(text.contains(location)){
				stop = el;
				break;
			}
		}
		Actions action = new Actions(driver);
		action.moveToElement(stop).click().build().perform();
		
	}

	public void selectFromDropDownOptionsDockIcon(String location) {
		log.info("Select location from drop down options");
		for(WebElement el : stops){
			String text = el.getText();
			boolean d = dockStationIcon.isDisplayed();
			if(text.contains(location)){
				stop = el;
				break;
			}
		}
		Actions action = new Actions(driver);
		action.moveToElement(stop).click().build().perform();
		
	}
	
	public NearbyResultSectionMap getNearbyResultSectionMap() {
		return new NearbyResultSectionMap(driver);
	}


}

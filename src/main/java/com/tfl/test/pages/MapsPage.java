package com.tfl.test.pages;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.slf4j.Logger;

import com.tfl.test.utils.common.ConfigUtils;

public class MapsPage {
	private static Logger log = null;
	static{
		if(log == null){
			log = ConfigUtils.getLogger(MapsPage.class);
		}
		 
}
	private WebDriver driver;
	
	@FindBy(how=How.CSS, using=".bus")
	WebElement Bus;
	
	@FindBy(how=How.CSS, using=".headline-container.plain>h1")
	WebElement busSpiderMapsHeadline;
	
	@FindBy(how=How.XPATH, using=".//*[@id='full-width-content']/div[3]/div[1]/div[3]/div[1]/ul/li/a")
	WebElement viewSpiderMapsText;
	
	@FindBy(how=How.XPATH, using=".//*[@id='full-width-content']/div[3]/div[1]/div/form/div/div[2]/input")
	WebElement viewFindSpiderMapsText;
	
	@FindBy(how=How.XPATH, using=".//*[@id='full-width-content']/div[1]/div/ul/li[4]/span")
	WebElement homeHeadlineView;
	
	@FindBy(how=How.XPATH, using=".//*[@id='full-width-content']/div[3]/div[1]/div/p[1]")
	WebElement articleTeaser;
	
	@FindBy(how=How.CSS, using=".last-breadcrumb")
	WebElement lastbreadcrumb;
	
	public MapsPage(WebDriver driver) {
        ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 30);
        PageFactory.initElements(finder, this);
        this.driver = driver;
	}
	public boolean checkWeAreInHomePage() {
		String title = driver.getTitle();
		log.info(title);
		return title.equals("Maps");
	}
	public boolean checkViewSpiderMapsTextShownInButton() {
		String text = viewSpiderMapsText.getText();
		log.info("Text : " + text);
		boolean contains = text.contains("View Spider Maps");
		return contains;
	}
	
	public void clickOnBusFromMaps(){
		Bus.click();
	}
	public void clickOnBusSpiderMaps(){
		viewSpiderMapsText.click();
		
	}
	public boolean checkHeadlineBusSpiderMapsText() {
		String text = busSpiderMapsHeadline.getText();
		log.info("Text : " + text);
		boolean contains = text.contains("Bus Spider Maps");
		return contains;
	}
	
	public boolean checkFindSpiderMapsText() {
		String value = viewFindSpiderMapsText.getAttribute("value");
		log.info("Text : " + value);
		boolean contains = value.contains("Find Spider Maps");
		return contains;
	}
	public boolean checkHomeHeadViewText() {
		String text = homeHeadlineView.getText();
		log.info("Text : " + text);
		boolean contains = text.contains("Bus Spider Maps");
		return contains;
	}
	public boolean checkArticalDescriptionText() {
		String text = articleTeaser.getText();
		log.info("Text : " + text);
		boolean contains = text.contains("Bus spider maps");
		return contains;
	}
}
package com.tfl.test.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.slf4j.Logger;

import com.tfl.test.utils.common.ConfigUtils;

/**
 * @author Noor
 */
public class WidgetsPage {
	
	private static Logger log = null;
	static{
		if(log == null){
			log = ConfigUtils.getLogger(SSPPage.class);
		}
	}
	@FindBy(how = How.ID, using = "JpEmbedPanel")
	private WebElement jpPanel;

	@FindBy(how = How.XPATH, using = "//*[@id='JpEmbedPanel']/a")
	private WebElement addLink;

	@FindBy(how = How.XPATH, using = ".//*[@id='aspnetForm']/h2[4]")
	private WebElement roads;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='full-width-content']/div/div/div[1]/h1")
	private WebElement widgets;
	
	private WebDriver driver;

	public WidgetsPage(WebDriver driver) {
		ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 30);
		PageFactory.initElements(finder, this);
		this.driver = driver;
	}

	public boolean checkWeAreTrackWidgetsPage() {
		return jpPanel.isDisplayed();
	}

	public void addJPWidget() {
		addLink.click();
	}
	public boolean checkRoadsText() {
		String text = roads.getText();
		log.info("Text : " + text);
		boolean contains = text.contains("Roads");
		return contains;
	}
	public boolean checkWidgetsText() {
		String text = widgets.getText();
		log.info("Text : " + text);
		boolean contains = text.contains("Widgets");
		return contains;
	}
}

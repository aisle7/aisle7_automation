package com.tfl.test.pages;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;

import com.tfl.test.domain.Disruption;
import com.tfl.test.domain.Line;
import com.tfl.test.utils.common.CIPWebUtils;
import com.tfl.test.utils.common.ConfigUtils;

/**
 * @author Noor
 */
public class StatusUpdatesPage {
	
	private static Logger log = null;
	static{
		if(log == null){
			log = ConfigUtils.getLogger(StatusUpdatesPage.class);
		}
	}
    @FindBy(how = How.CSS, using = "input.primary-button.plan-journey-button")
    private WebElement planAJourneyButton;
    
    @FindBy(how = How.CSS, using = "#Input")
    private WebElement busInput;

    @FindBy(how = How.ID, using="From")
    private WebElement fromStation;

    @FindBy(how = How.ID, using="To")
    private WebElement toStation;
    
    @FindBy(how = How.CLASS_NAME, using="modes-dropdown-placeholder")
    private WebElement modeDropDown;
    
    @FindBy(how = How.CLASS_NAME, using="date-dropdown-placeholder")
    private WebElement dateDropDown;

	@FindBy(how=How.CLASS_NAME, using="section")
	List<WebElement> sections;

	@FindBy(how=How.CLASS_NAME, using="disruption-summary")
	WebElement dsummary;

	@FindBy(how=How.ID, using="trammap")
	WebElement tramMap;
	
	@FindBy(how=How.ID, using="cablecarmap")
	WebElement emiratesMap;
	
	@FindBy(how=How.ID, using="rivermap")
	WebElement riverBusMap;
	
	@FindBy(how=How.ID, using="lul-bakerloo")
	WebElement bakerloo;
	
	@FindBy(how=How.ID, using="lul-central")
	WebElement central;
	
	@FindBy(how=How.XPATH, using=".//*[@id='disambiguation-options']/li[2]/a")
	WebElement option2FromList;

	@FindBy(how=How.CSS, using=".primary-button")
	WebElement goButton;
	
    private WebDriver driver;

    public StatusUpdatesPage(WebDriver driver) {
        ElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 30);
        PageFactory.initElements(finder, this);
        this.driver = driver;
    }

    public boolean checkWeAreTrackStatusUpdatesPage() {
        String title = driver.getTitle();
        log.info("driver page status"+title + driver.getCurrentUrl());
        return title.equals("Tube, DLR, Overground status updates - Transport for London");
    }

    public void selectMode(String mode){
		clickOnModeDropDown();
		mode = CIPWebUtils.getModeId(mode);
		selectSpecifiedMode(mode);
    }

	public StatusUpdatesPage selectDate(String dateText) {
		clickOnDateDropDown();
		selectSpecifiedDate(dateText);
		return new StatusUpdatesPage(driver);
	}

	private void selectSpecifiedDate(String date) {
		String dateId = "";
		if(date.contains("Later")){
			dateId = CIPWebUtils.getLaterToday().replace("201", "1");
		}else if(date.contains("weekend")){
			dateId = CIPWebUtils.getWeekendDateId().replace("201", "1");
		}else if(date.contains("Christmas")){
			dateId = CIPWebUtils.getChristmasDay().replace("201", "1");
		}
		log.info(dateId);
		//wait for options 
		WebElement stop = driver.findElement(By.cssSelector("li[data-item-id='" + dateId + "']"));
		new WebDriverWait(driver, 2).until(ExpectedConditions.elementToBeClickable(stop));
		
		//driver.findElement(By.cssSelector(".no-js-next-month")).click();
		//WebElement dataselecteditemid = driver.findElement(By.cssSelector("data-selected-item-id='" + dateId + "']"));
		//new WebDriverWait(driver, 2).until(ExpectedConditions.elementToBeClickable(dataselecteditemid));
		 

		//WebElement featureDate = driver.findElement(By.className("advance-month-container").cssSelector("data-item-id='"+ dateId + "'"));
		//new WebDriverWait(driver, 2).until(ExpectedConditions.elementToBeClickable(featureDate));
		
		//hover and click
		Actions action = new Actions(driver);
		action.moveToElement(stop).clickAndHold().release().build().perform();
		//action.moveToElement(dataselecteditemid).clickAndHold().release().build().perform();
	}

	private void selectSpecifiedMode(String mode) {
		//wait for options 
		WebElement stop = driver.findElement(By.className(mode));
		new WebDriverWait(driver, 5).until(ExpectedConditions.elementToBeClickable(stop));
		
		//hover and click
		Actions action = new Actions(driver);
		action.moveToElement(stop).clickAndHold().release().build().perform();
	}


	private void clickOnModeDropDown() {
		modeDropDown.click();
	}

	public void enterBusInput(){
		busInput.sendKeys("95");
		
	}
	
	public void goButtonClick(){
		goButton.click();
	}
	public void option2fromBusInput(){
		option2FromList.click();
	}

	private void clickOnDateDropDown() {
		dateDropDown.click();
	}
	
    public boolean checkThisLineIsDisplayed(String lineName, List<Line> lines) {
        boolean isDisplayed = false;
        for (Line line:lines) {
            if (StringUtils.equalsIgnoreCase(line.getName(), lineName) && line.getStatusSeverityDescription().length() > 0) {
                isDisplayed = true;
                break;
            }
        }
        if (!isDisplayed) {
            log.info(lineName + "Line is not displayed ");
        }
        return  isDisplayed;
    }

    public static List<Line> getLineStatusDTO(WebDriver driver) {
        String xPathForDisruptedService = ".//*[@id='rainbow-list-tube-dlr-overground']/ul/li/a";
        List<WebElement> disruptedLines = driver.findElements(By.xpath(xPathForDisruptedService));
        log.info(""+disruptedLines.size());
        String xPathForAllLines = ".//*[@id='rainbow-list-tube-dlr-overground']/ul/li/div";
        List<WebElement> allServiceLines = driver.findElements(By.xpath(xPathForAllLines));
        log.info(""+allServiceLines.size());
        List<Line> lineDTOFromWeb = new ArrayList<Line>();
        String lineName ="";
        String lineStatus = "";
        String xPath = "";
        for (int i=0; i<disruptedLines.size(); i++) {
            ///a/span[1]/span
            xPath = ".//*[@id='rainbow-list-tube-dlr-overground']/ul/li[" + (i+1) +"]/a/span[1]/span";
            lineName = driver.findElement(By.xpath(xPath)).getText();
            xPath = ".//*[@id='rainbow-list-tube-dlr-overground']/ul/li[" + (i+1) +"]/a/span[2]/span";
            lineStatus = driver.findElement(By.xpath(xPath)).getText();
            Line line = new Line();
            line.setName(lineName);
            line.setStatusSeverityDescription(lineStatus);
            lineDTOFromWeb.add(line);
        }
        int goodServiceStartIndex = disruptedLines.size();
        for (int i=goodServiceStartIndex; i<allServiceLines.size(); i++) {
            ///a/span[1]/span
            xPath = ".//*[@id='rainbow-list-tube-dlr-overground']/ul/li[" + (i+1) +"]/div/span[1]/span";
            lineName = driver.findElement(By.xpath(xPath)).getText();
            xPath = ".//*[@id='rainbow-list-tube-dlr-overground']/ul/li[" + (i+1) +"]/div/span[2]/span";
            lineStatus = driver.findElement(By.xpath(xPath)).getText();
            Line line = new Line();
            line.setName(lineName);
            line.setStatusSeverityDescription(lineStatus);
            lineDTOFromWeb.add(line);
        }
        return lineDTOFromWeb;
    }

	public Line expandARandomDisruption(String mode) {
		mode = getModeIdInApi(mode);
		Line line = CIPWebUtils.getRandomLineWithDisruption(mode);
		if(line!=null){
			String lineId = "lul-" + line.getId();
			WebElement li = driver.findElement(By.cssSelector("li[data-line-id='" + lineId + "']"));
			li.click();
		}
		return line;
	}


	

	public boolean checkAPIDataMatchesWithDisplayedStatusSeverity(Line line) {
		boolean matches = false;
		
		if(line!=null){
			String statusSeverityDescription = line.getStatusSeverityDescription().toLowerCase();
			String lineId = "lul-" + line.getId();
			
			WebElement li = driver.findElement(By.cssSelector("li[data-line-id='" + lineId + "']"));
			WebElement dsummary = li.findElement(By.className("disruption-summary"));
			String summary = dsummary.getText().toLowerCase();
			
			matches = summary.contains(statusSeverityDescription);
		}
		return matches;
	}

	public boolean checkAPIDataMatchesWithDisplayedDisruptionMessage(Line line) {
		boolean matches = false;
		if(line!=null){
			Disruption disruption = CIPWebUtils.getRandomDisruptionFromLine(line.getDisruptionList());
			if(disruption!=null){
				String lineId = "lul-" + line.getId();
				WebElement li = driver.findElement(By.cssSelector("li[data-line-id='" + lineId + "']"));
				matches = checkDisruptionMessagesMatchBetweenAPIAndWeb(li, disruption);
			}else{
				//No disruptions therefore nothing to check
				matches = true;
			}
		}
		return matches;
	}
	
	private boolean checkDisruptionMessagesMatchBetweenAPIAndWeb(WebElement li,
			Disruption disruption) {
		boolean matched = false;

		for(WebElement el: sections){
			String disruptionsText = el.getText().toLowerCase();
			String text = disruption.getReason().toLowerCase();
			if(disruptionsText.contains(text)){
				matched = true;
				break;
			}
		}
		return matched;
	}

	public Line expandALineDisruption(String lineName, String mode) {
		mode = getModeIdInApi(mode);
		List<Line> lines = CIPWebUtils.getListOfLineWithDisruption(mode);
		Line line = getSpecifiedLine(lines, lineName);
		if(line!=null){
			String lineId = "lul-" + line.getId();
			WebElement li = driver.findElement(By.cssSelector("li[data-line-id='" + lineId + "']"));
			li.click();
		}
		return line;
	}

	private Line getSpecifiedLine(List<Line> lines, String lineName) {
		Line line = null;
		for(Line l: lines){
			String name = l.getName();
			if(name.equals(lineName)){
				line = l;
				break;
			}
		}
		return line;
	}

	public boolean checkCorrectMapIsDsiplayed(String mapType) {
		if(mapType.equals("Tram")){
			return tramMap.isDisplayed();
		}else if(mapType.equals("Emirates Air Line")){
			return emiratesMap.isDisplayed();
		}else if(mapType.equals("River Bus")){
			return riverBusMap.isDisplayed();
		} 
		return false;
	}
	
	public boolean checkCorrectTubeIsDsiplayed(String mapType) {
		if(mapType.equals("Bakerloo")){
			return bakerloo.isDisplayed();
		}else if(mapType.equals("Central")){
			return central.isDisplayed();
		} 
		return false;
	}

	private String getModeIdInApi(String mode) {
		if(mode.equals("Tram")){
			return "tram";
		}else if(mode.equals("Emirates Air Line")){
			return "cable-car";
		}else if(mode.equals("River Bus")){
			return "river-bus";
		} 
		return mode;
	}
	
}
